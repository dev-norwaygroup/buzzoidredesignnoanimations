/**
 * External Dependencies
 */
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';

const schema = yup.object().shape({
  email: yup
    .string()
    .email('Invalid email')
    .required('Please enter your email'),
  name: yup.string().required('Please enter your name'),
  message: yup.string().required('Please enter a message'),
});

const ContactForm = () => {
  const [showSuccessMessage, setShowSuccessMessage] = useState(false);
  const [showFailureMessage, setShowFailureMessage] = useState(false);
  const [buttonText, setButtonText] = useState('Send');

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmitHandler = async (data) => {
    setButtonText('Sending');
    const { email, name, subject, orderNumber, message } = data;
    const res = await fetch('/api/sendgrid', {
      body: JSON.stringify({
        email: email,
        name: name,
        message: message,
        subject: subject ? subject : 'Empty',
        orderNumber: orderNumber ? orderNumber : 'Empty',
      }),
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
    });

    const { error } = await res.json();
    if (error) {
      setShowFailureMessage(true);
      setButtonText('Send');
      return;
    }
    setShowSuccessMessage(true);
    setButtonText('Send');
    reset();
  };

  return (
    <div className="form contact-form">
      <form onSubmit={handleSubmit(onSubmitHandler)}>
        <div className="form__head">
          <h1>Contact Us</h1>
          <p>Ask us if you have any questions. We can help!</p>
        </div>
        <div className="form__body">
          <div className="form__row">
            <label htmlFor="name" className="form__label required">
              Your name
            </label>
            <input
              {...register('name')}
              type="text"
              className="form__field"
              name="name"
              id="name"
              placeholder="Your Name"
            />
            {errors.name && <p className="error">{errors.name.message}</p>}
          </div>
          <div className="form__row">
            <label htmlFor="email" className="form__label required">
              Your e-mail
            </label>
            <input
              {...register('email')}
              type="text"
              className="form__field"
              name="email"
              id="email"
              placeholder="example@example.com"
            />
            {errors.email && <p className="error">{errors.email.message}</p>}
          </div>
          <div className="form__row">
            <label htmlFor="order-number" className="form__label">
              Order number
            </label>
            <input
              {...register('order-number')}
              type="text"
              className="form__field"
              id="order-number"
              name="order-number"
              placeholder="10000123"
            />
            {errors.orderNumber && (
              <p className="error">{errors.orderNumber.message}</p>
            )}
          </div>
          <div className="form__row">
            <label htmlFor="subject" className="form__label">
              Subject
            </label>
            <input
              {...register('subject')}
              type="text"
              className="form__field"
              id="subject"
              name="subject"
              placeholder="Subject"
            />
            {errors.subject && (
              <p className="error">{errors.subject.message}</p>
            )}
          </div>
          <div className="form__row last">
            <label htmlFor="message" className="form__label required">
              Message
            </label>
            <textarea
              {...register('message')}
              id="message"
              className="form__field form__field--textarea"
              name="message"
              placeholder="Type a message..."
            />
            {errors.message && (
              <p className="error">{errors.message.message}</p>
            )}
          </div>
        </div>
        <div className="form__actions">
          <input type="submit" className="btn btn--orange" value={buttonText} />
          {showSuccessMessage && (
            <p className="success">
              Thank you! Your message has been delivered.
            </p>
          )}
          {showFailureMessage && (
            <p className="error">Something went wrong...</p>
          )}
        </div>
      </form>
    </div>
  );
};

export default ContactForm;

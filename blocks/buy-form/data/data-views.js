import { Trans } from '@lingui/macro';

const ViewsData = [
  {
    type: 'radios',
    name: 'views',
    value: [
      {
        value: '500',
        label: (
          <span>
            <Trans>views:</Trans>
            <strong>
              $1.99 <strike>$2.21</strike>
            </strong>
          </span>
        ),
        visible: true,
        popular: false,
        save: '10%',
        saved: '$0.22',
      },
      {
        value: '2500',
        label: (
          <span>
            <Trans>views:</Trans>
            <strong>
              $6.99 <strike>$9.08</strike>
            </strong>
          </span>
        ),
        visible: true,
        popular: false,
        save: '23%',
        saved: '$2.09',
      },
      {
        value: '5000',
        label: (
          <span>
            <Trans>views:</Trans>
            <strong>
              $14.99 <strike>$23.42</strike>
            </strong>
          </span>
        ),
        visible: true,
        popular: false,
        save: '36%',
        saved: '$8.43',
      },
      {
        value: '10000',
        label: (
          <span>
            <Trans>views:</Trans>
            <strong>
              $24.99 <strike>$49.00</strike>
            </strong>
          </span>
        ),
        visible: false,
        popular: false,
        save: '49%',
        saved: '$24.01',
      },
      {
        value: '25000',
        label: (
          <span>
            <Trans>views:</Trans>
            <strong>
              $49.99 <strike>$131.55</strike>
            </strong>
          </span>
        ),
        visible: false,
        popular: false,
        save: '62%',
        saved: '$81.56',
      },
      {
        value: '50000',
        label: (
          <span>
            <Trans>views:</Trans>
            <strong>
              $74.99 <strike>$299.96</strike>
            </strong>
          </span>
        ),
        visible: false,
        popular: false,
        save: '75%',
        saved: '$224.97',
      },
    ],
  },
];

export default ViewsData;

import { Trans } from '@lingui/macro';

const DataAutomatic = [
  {
    type: 'radios',
    name: 'likes',
    value: [
      {
        value: '50',
        label: (
          <span>
            <Trans>likes per post</Trans>
            <strong>
              $9.99 / <Trans>mo</Trans>
            </strong>
          </span>
        ),
        visible: true,
        popular: false,
        save: false,
        saved: false,
      },
      {
        value: '100',
        label: (
          <span>
            <Trans>likes per post</Trans>
            <strong>
              $19.99 / <Trans>mo</Trans>
            </strong>
          </span>
        ),
        visible: true,
        popular: true,
        save: '10%',
        saved: '$3.32',
      },
      {
        value: '250',
        label: (
          <span>
            <Trans>likes per post</Trans>
            <strong>
              $12.99 / <Trans>mo</Trans>
            </strong>
          </span>
        ),
        visible: true,
        popular: false,
        save: '15%',
        saved: '$4.32',
      },
      {
        value: '500',
        label: (
          <span>
            <Trans>likes per post</Trans>
            <strong>
              $29.99 / <Trans>mo</Trans>
            </strong>
          </span>
        ),
        visible: false,
        popular: false,
        save: '37%',
        saved: '$4.32',
      },
      {
        value: '1000',
        label: (
          <span>
            <Trans>likes per post</Trans>
            <strong>
              $39.99 / <Trans>mo</Trans>
            </strong>
          </span>
        ),
        visible: false,
        popular: false,
        save: '37%',
        saved: '$4.32',
      },
      {
        value: '2500',
        label: (
          <span>
            <Trans>likes per post</Trans>
            <strong>
              $49.99 / <Trans>mo</Trans>
            </strong>
          </span>
        ),
        visible: false,
        popular: false,
        save: '37%',
        saved: '$4.32',
      },
      {
        value: '5000',
        label: (
          <span>
            <Trans>likes per post</Trans>
            <strong>
              $59.99 / <Trans>mo</Trans>
            </strong>
          </span>
        ),
        visible: false,
        popular: false,
        save: '37%',
        saved: '$4.32',
      },
    ],
  },
];

export default DataAutomatic;

/**
 * External dependencies
 */
import { Trans } from '@lingui/macro';

/**
 * Internal dependencies
 */
import IconTooltip from '@/svg/icon-tooltip.svg';
import IconClose from '@/svg/icon-close.svg';

const BuylikesFormInlineRadio = ({ fields, name, onChange, toggleTooltip, isTooltipOpen }) => {
  return (
    <div className="form-fields">
      <div className="inline-radio-outer">
        <div className="inline-radio-recommended">
          <Trans>Recommended</Trans>
        </div>
        <button
          type="button"
          className="inline-radio-tooltip__button"
          onClick={toggleTooltip}
        >
          <IconTooltip />
        </button>
        {isTooltipOpen && (
          <div className="inline-radio-tooltip__dropdown">
            <button
              type="button"
              className="inline-radio-tooltip__dropdown-close"
              onClick={toggleTooltip}
            >
              <IconClose />
            </button>
            {fields.map((field, index) => (
              <div className="inline-radio-tooltip__dropdown-text" key={index}>
                <p>
                  <strong>{field.value}</strong>
                </p>
                <p>{field.description}</p>
              </div>
            ))}
          </div>
        )}

        <div className="inline-radio-holder">
          {fields.map(({ value, checked }) => (
            <div className="inline-radio" key={value}>
              <input
                type="radio"
                name={name}
                value={value}
                defaultChecked={checked}
                onChange={onChange}
              />
              <div className="inline-radio__value">{value}</div>
              <div className="inline-radio__bg" />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default BuylikesFormInlineRadio;

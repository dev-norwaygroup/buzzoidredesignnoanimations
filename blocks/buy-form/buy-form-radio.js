/**
 * External Dependencies
 */
import { Trans } from '@lingui/macro';

/**
 * Internal dependencies
 */

import IconChevronDown from '@/svg/icon-chevron-down.svg';
import IconCheckmark from '@/svg/icon-checkmark.svg';

const BuylikesFormRadio = ({ fields, onChange, name, fieldValue, prefix }) => {
  return (
    <div className="radio-holder">
      {fields.map(
        ({ value, label, visible, popular, save, saved }) =>
          visible && (
            <div className="radio" key={value}>
              <input
                type="radio"
                name={name}
                value={value}
                checked={fieldValue === value}
                id={`radio-${prefix}-${value}`}
                onChange={onChange}
              />
              <span className="radio__item"></span>
              <label htmlFor={`radio-${prefix}-${value}`}>
                {value} {label}
              </label>
              <div className="radio__side">
                {popular && <span className="radio__popular">Popular</span>}
                {save && (
                  <span className="radio__save">
                    <Trans>Save</Trans> {save}
                  </span>
                )}
                {saved && (
                  <div className="radio__saved">
                    <div className="radio__saved-icon">
                      <IconCheckmark />
                    </div>
                    <div className="radio__saved-text">
                      <Trans>Saved</Trans>
                      <strong>{saved}</strong>
                    </div>
                  </div>
                )}
              </div>
              <div className="radio__bg"></div>
            </div>
          )
      )}
    </div>
  );
};

export default BuylikesFormRadio;

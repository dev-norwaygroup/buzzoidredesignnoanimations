/**
 * External Dependencies
 */
import { useState, useEffect } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { Trans } from '@lingui/macro';

/**
 * Internal dependencies
 */

import BuyFormAltColumns from './buy-form-alt-columns';
import BuyFormAltGridRadios from './buy-form-alt-grid-radios';
import IconCheckSaved from '@/svg/icon-check-saved.svg';
import FlexRow from '@/components/flex-row/flex-row';

const BuyFormAlt = ({ fields, schema }) => {
  const [active, setActive] = useState(false);
  const closeTooltip = () => setActive(false);
  const toggleTooltip = () => setActive(!active);

  const handleToggleClick = () => {
    if (active) {
      setActive(false);
    }
  };

  useEffect(() => {
    if (typeof window === 'object') {
      window.addEventListener('click', handleToggleClick);
    }
    return () => window.removeEventListener('click', handleToggleClick);
  });

  const defaultValues = fields.reduce((acc, val) => {
    if (val.value.filter((item) => (item.checked ? true : false))) {
      return {
        ...acc,
        [val.name]: val.value.filter((item) => item.checked)[0]?.value,
      };
    }
    return acc;
  }, {});
  const {
    control,
    resetField,
    watch,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: 'onChange',
    defaultValues,
    resolver: yupResolver(schema),
  });
  const [price, setPrice] = useState();
  const handlePrice = (data) => setPrice(data);
  const onSubmit = (data) => console.log(data);

  return (
    <div className="form buy-form-alt">
      <form onSubmit={handleSubmit(onSubmit)}>
        {fields.map((item) => {
          switch (item.type) {
            case 'inline-radios':
              return (
                <Controller
                  key={item.name}
                  render={({ field }) => (
                    <div className="radio-columns-holder">
                      <BuyFormAltColumns
                        active={active}
                        toggleTooltip={toggleTooltip}
                        fields={item.value}
                        field={field}
                        name={item.name}
                        key={item.type}
                        onChange={(e) => {
                          field.onChange(e);
                          handlePrice(null);
                          closeTooltip();
                          resetField(
                            fields.find((x) => x.name !== item.name).name
                          );
                        }}
                      />
                      {errors[item.name]?.message && (
                        <p className="error">{errors[item.name].message}</p>
                      )}
                    </div>
                  )}
                  control={control}
                  name={item.name}
                />
              );
            case 'radios':
              if (item.condition) {
                const typeWatch = watch(item.condition.type);
                if (typeWatch != item.condition.value) {
                  return;
                }
              }
              return (
                <Controller
                  key={item.name}
                  render={({ field }) => (
                    <div className="grid-radios-holder">
                      <BuyFormAltGridRadios
                        fields={item.value}
                        value={field.value}
                        name={item.name}
                        key={item.type}
                        handlePrice={handlePrice}
                        onChange={field.onChange}
                      />
                      {errors[item.name]?.message && (
                        <p className="error">{errors[item.name].message}</p>
                      )}
                    </div>
                  )}
                  control={control}
                  name={item.name}
                />
              );
            default:
              return null;
          }
        })}
        <div className="flex-row align-center buy-form-alt__actions">
          <div className="flex-col">
            {price && (
              <div className="buy-form-alt__price">
                <h4>
                  <sup>$</sup>
                  {price.price}
                  <sup className="strike">{price.old}</sup>
                </h4>
                {price.saved && (
                  <FlexRow className="align-center">
                    <FlexRow.Col>
                      <IconCheckSaved />
                    </FlexRow.Col>
                    <FlexRow.Col>
                      <span className="buy-form-alt__saved">
                        <Trans>You Saved</Trans> <strong>{price.saved}</strong>
                      </span>
                    </FlexRow.Col>
                  </FlexRow>
                )}
              </div>
            )}
          </div>
          <div className="flex-col">
            <button type="submit" className="btn btn--orange">
              Buy Now
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default BuyFormAlt;

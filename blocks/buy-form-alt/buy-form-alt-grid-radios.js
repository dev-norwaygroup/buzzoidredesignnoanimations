/**
 * External Dependencies
 */
import { Trans } from '@lingui/macro';

const BuyFormAltGridRadios = ({
  fields,
  name,
  handlePrice,
  onChange,
  value,
}) => {
  return (
    <div className="grid-radios">
      {fields.map((field, index) => {
        const { save } = field;
        return (
          <div className="grid-radios__item" key={index}>
            <input
              type="radio"
              name={name}
              value={field.value}
              id={`radio-${field.value}`}
              checked={field.value === value}
              onChange={(e) => {
                handlePrice(field);
                onChange(e);
              }}
            />
            <label htmlFor={`radio-${field.value}`}>
              {field.value} {!save && <span>{name}</span>}
            </label>
            {save && (
              <div className="grid-radios__item-save">
                {save} <Trans>off</Trans>
              </div>
            )}
            <div className="grid-radios__item-bg"></div>
          </div>
        );
      })}
    </div>
  );
};

export default BuyFormAltGridRadios;

import { Trans } from '@lingui/macro';

const FollowersAltData = [
  {
    type: 'inline-radios',
    name: 'type',
    value: [
      {
        value: 'High Quality Followers',
        tooltip: false,
        checked: true,
        color: 'orange',
        list: [
          {
            value: (
              <Trans>
                <strong>High Quality</strong>
                <br /> Followers
              </Trans>
            ),
          },
          {
            value: (
              <Trans>
                <strong>Fast</strong> Delivery
              </Trans>
            ),
          },
          {
            value: (
              <Trans>
                <strong>No password</strong>
                <br /> required
              </Trans>
            ),
          },
          {
            value: (
              <Trans>
                <strong>24/7</strong> support
              </Trans>
            ),
          },
        ],
        description: (
          <Trans>
            These are Likes with profile pictures but no further uploads on
            their account. Auto-refill is enabled within the warranty.
          </Trans>
        ),
      },
      {
        value: 'Active Followers',
        checked: false,
        tooltip: true,
        color: 'blue',
        list: [
          {
            value: (
              <Trans>
                <strong>Real Active</strong> Followers
              </Trans>
            ),
          },
          {
            value: (
              <Trans>
                Guaranteed <strong>Instant Delivery</strong>
              </Trans>
            ),
          },
          {
            value: (
              <Trans>
                <strong>No password</strong>
                <br /> required
              </Trans>
            ),
          },
          {
            value: (
              <Trans>
                <strong>30 day</strong> refills
              </Trans>
            ),
          },
          {
            value: (
              <Trans>
                <strong>24/7</strong> support
              </Trans>
            ),
          },
        ],
        description: (
          <Trans>
            Buzzoid is now the only website to offer active likes. That&apos;s
            right — 100% real Instagram likes, from 100% real Instagram users!
            Likes from real people really interested in your content. NO Drops!
          </Trans>
        ),
      },
    ],
  },
  {
    type: 'radios',
    name: 'followers',
    condition: {
      type: 'type',
      value: 'High Quality Followers',
    },
    value: [
      {
        value: '100',
        price: '2.97',
        old: '$3.30',
        save: false,
        saved: '$0.33',
      },
      {
        value: '250',
        price: '4.99',
        old: '$7.43',
        save: '23%',
        saved: '$2.44',
      },
      {
        value: '500',
        price: '6.99',
        old: '$14.85',
        save: '36%',
        saved: '$7.86',
      },
      {
        value: '1000',
        price: '12.99',
        old: '$29.70',
        save: '49%',
        saved: '$16.71',
      },
      {
        value: '2500',
        price: '29.99',
        old: '$74.25',
        save: '62%',
        saved: '$44.26',
      },
      {
        value: '5000',
        price: '39.99',
        old: '$148.50',
        save: '75%',
        saved: '$108.51',
      },
    ],
  },
  {
    type: 'radios',
    name: 'followers',
    condition: {
      type: 'type',
      value: 'Active Followers',
    },
    value: [
      {
        value: '500',
        price: '11.99',
        old: '$15.99',
        save: false,
        saved: '$4.00',
      },
      {
        value: '1000',
        price: '19.99',
        old: '$45.43',
        save: '49%',
        saved: '$25.44',
      },
      {
        value: '2500',
        price: '49.99',
        old: '$124.98',
        save: '62%',
        saved: '$74.99',
      },
      {
        value: '5000',
        price: '84.99',
        old: '$314.78',
        save: '62%',
        saved: '$229.79',
      },
    ],
  },
];

export default FollowersAltData;

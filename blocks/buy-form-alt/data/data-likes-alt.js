import { Trans } from '@lingui/macro';

const LikesAltData = [
  {
    type: 'inline-radios',
    name: 'type',
    value: [
      {
        value: 'High Quality Likes',
        tooltip: false,
        checked: true,
        color: 'orange',
        list: [
          {
            value: (
              <Trans>
                <strong>Real</strong> likes from <strong>Real</strong> people{' '}
                <a href="#">What&#39;s the difference?</a>
              </Trans>
            ),
          },
          {
            value: (
              <Trans>
                Guaranteed <strong>Instant Delivery</strong>
              </Trans>
            ),
          },
          {
            value: (
              <Trans>
                Option to <strong>split likes</strong> on multiple pictures
              </Trans>
            ),
          },
          {
            value: <Trans>Includes video views</Trans>,
          },
          {
            value: (
              <Trans>
                <strong>No password</strong>
                <br /> required
              </Trans>
            ),
          },
          {
            value: (
              <Trans>
                Fast Delivery <strong>(gradual or instant)</strong>
              </Trans>
            ),
          },
          {
            value: (
              <Trans>
                <strong>24/7</strong> support
              </Trans>
            ),
          },
        ],
        description: (
          <Trans>
            These are Likes with profile pictures but no further uploads on
            their account. Auto-refill is enabled within the warranty.
          </Trans>
        ),
      },
      {
        value: 'Premium Likes',
        checked: false,
        tooltip: true,
        color: 'blue',
        list: [
          {
            value: (
              <Trans>
                <strong>Real</strong> likes from <strong>Active</strong>{' '}
                profiles
                <br />
                <a href="#">What&#39;s the difference?</a>
              </Trans>
            ),
          },
          {
            value: (
              <Trans>
                Guaranteed <strong>Instant Delivery</strong>
              </Trans>
            ),
          },
          {
            value: (
              <Trans>
                Option to <strong>split likes</strong> on multiple pictures
              </Trans>
            ),
          },
          {
            value: <Trans>Includes video views</Trans>,
          },
          {
            value: (
              <Trans>
                <strong>No password</strong>
                <br /> required
              </Trans>
            ),
          },
          {
            value: (
              <Trans>
                Fast Delivery <strong>(gradual or instant)</strong>
              </Trans>
            ),
          },
          {
            value: (
              <Trans>
                <strong>24/7</strong> support
              </Trans>
            ),
          },
          {
            highlighted: true,
            value: <Trans>Increased change to reach explore page</Trans>,
          },
        ],
        description: (
          <Trans>
            Buzzoid is now the only website to offer active likes. That's right
            — 100% real Instagram likes, from 100% real Instagram users! Likes
            from real people really interested in your content. NO Drops!
          </Trans>
        ),
      },
    ],
  },
  {
    type: 'radios',
    name: 'likes',
    value: [
      {
        value: '50',
        price: '1.47',
        old: '$1.63',
        save: false,
        saved: false,
      },
      {
        value: '100',
        price: '2.97',
        old: '$3.67',
        save: '19%',
        saved: '$3.32',
      },
      {
        value: '250',
        price: '4.99',
        old: '$6.93',
        save: '28%',
        saved: '$4.32',
      },
      {
        value: '500',
        price: '6.99',
        old: '$11.10',
        save: '37%',
        saved: '$4.32',
      },
      {
        value: '1000',
        price: '12.99',
        old: '$24.06',
        save: '46%',
        saved: '$4.32',
      },
      {
        value: '2500',
        price: '24.99',
        old: '$55.53',
        save: '55%',
        saved: '$4.32',
      },
      {
        value: '5000',
        price: '44.99',
        old: '$124.97',
        save: '64%',
        saved: '$4.32',
      },
      {
        value: '10000',
        price: '88.99',
        old: '$355.96',
        save: '75%',
        saved: '$4.32',
      },
    ],
  },
];

export default LikesAltData;

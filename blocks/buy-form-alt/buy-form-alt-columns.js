/**
 * External dependencies
 */
import classNames from 'classnames';

/**
 * Internal dependencies
 */
import IconTooltip from '@/svg/icon-tooltip.svg';
import Check from '@/svg/icon-check2.svg';
import IconClose from '@/svg/icon-close.svg';

const BuyFormAltColumns = ({ fields, onChange, name, field, active, toggleTooltip }) => {
  return (
    <div className="radio-columns">
      {fields.map(({ value, checked, tooltip, list, color }, index) => (
        <div
          className={classNames('radio-column', color, {
            'is-active': value === field.value,
          })}
          key={index}
        >
          <input
            type="radio"
            name={name}
            defaultChecked={checked}
            value={value}
            onChange={onChange}
          />
          <div className="radio-column__check">
            <Check />
          </div>
          <div className="radio-column__header">{value}</div>
          {tooltip && (
            <>
              <button type="button" className="radio-column__button" onClick={toggleTooltip}>
                <IconTooltip />
              </button>
              {active && (
                <div className="inline-radio-tooltip__dropdown">
                  <button
                    type="button"
                    className="inline-radio-tooltip__dropdown-close"
                    onClick={toggleTooltip}
                  >
                    <IconClose />
                  </button>
                  {fields.map((field, index) => (
                    <div
                      className="inline-radio-tooltip__dropdown-text"
                      key={index}
                    >
                      <p>
                        <strong>{field.value}</strong>
                      </p>
                      <p>{field.description}</p>
                    </div>
                  ))}
                </div>
              )}
            </>
          )}
          <div className="radio-column__body">
            <ul className="radio-column__list">
              {list.map((field, ind) => (
                <li
                  key={ind}
                  className={classNames({ highlighted: field.highlighted })}
                >
                  <Check /> {field.value}
                </li>
              ))}
            </ul>
          </div>
        </div>
      ))}
    </div>
  );
};

export default BuyFormAltColumns;

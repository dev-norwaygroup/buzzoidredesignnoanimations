const homeReviews = [
    {
        text: "Instagram really helps to build our presence in social media. We were blow away by Buzzoid's speed and quality for likes and followers!",
        author: 'Chris Johnson'
    },
    {
        text: "Didn't get the followers instantly but The customer support is so fast! Totally worth",
        author: 'James'
    },
    {
        text: 'I swear this has to be the best website I have ever come across. I needed specifically just views for a video my friend made, and oh boy did they deliver. Thanks guys!',
        author: 'Jens'
    },
    {
        text: 'Visiting for the first time. Quick likes, speedy and worth-the-cash services is what I received.',
        author: 'Michael Manley'
    },
    {
        text: 'Made payment at night, woke up with three times the followers! Is it a dream? No, just Buzzoid! Hahaha, thanks again gentlemen. Top service you guys have here',
        author: 'Jack'
    },
    {
        text: 'Buzzoid has me spellbound. It provides such radical results at extremely cheap rates are unbelievable.',
        author: 'Cecil'
    },
    {
        text: 'Amazing works perfect and on time',
        author: 'ultimate_human_freedom'
    },
];

const likesReviews = [
    {
        text: "How did you make verified users with hundreds of thousands of followers to like my picture for such a cheap price? You should increase the prices on premium likes, seriously. I would be willing to pay way more for this service and keep the cheapskates out.",
        author: 'Vlad P'
    },
    {
        text: 'They are good and real',
        author: 'sagebonque'
    },
    {
        text: 'Actually worked and very professional',
        author: 'Harry'
    },
    {
        text: 'Awesome app and highly recommend',
        author: 'Jose Fernandez'
    },
    {
        text: 'Highly recommend this platform.',
        author: 'Anandhu'
    },
    {
        text: 'It works so well thank you so much !! Will order again !',
        author: 'Dan'
    },
    {
        text: 'Very good app and trusted',
        author: 'Akshat'
    },
    {
        text: 'Best app ever...it really gives you real likes',
        author: 'Vaibhav Donerao'
    }
];

const followersReviews = [
    {
        text: 'Love this app!',
        author: 'Ellie'
    },
    {
        text: 'better than all other websites and this one actually works!',
        author: 'andre'
    },
    {
        text: "Great service and like that you're given a trial before paying",
        author: 'Kay'
    },
    {
        text: 'Awesome app and highly recommend',
        author: 'Jose Fernandez'
    },
    {
        text: 'It actually works and all of them were legit !!',
        author: 'Jason'
    },
    {
        text: 'Really quick and easy',
        author: 'Bailey'
    },
    {
        text: 'Amazing, so helpful and has helped me improve my page and earn more followers with the positive engagement increase.',
        author: 'Matt Cromer'
    },
    {
        text: 'Great way to get followers',
        author: 'Kamerin carter'
    }
];

const viewsReviews = [
    {
        text: 'It worked really fast',
        author: 'Jaiden Tate'
    },
    {
        text: 'Thank you!! Very fast',
        author: 'Kiyo'
    },
    {
        text: "Very good thanks",
        author: 'Bahrom'
    },
    {
        text: 'Always worked excellent',
        author: 'Maya'
    },
    {
        text: 'Incredibly fast service',
        author: 'Greg'
    },
    {
        text: 'Absolutely amazing service',
        author: 'Dan'
    },
    {
        text: 'I love this app! Flawless',
        author: 'Roy'
    },
    {
        text: 'Great way to gain real followers',
        author: 'Warriorx_kingup'
    }
];


export {
    homeReviews,
    likesReviews,
    followersReviews,
    viewsReviews
}
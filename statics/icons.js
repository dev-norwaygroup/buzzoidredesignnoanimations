/**
 * External Dependencies
 */
import { Trans } from '@lingui/macro';

/**
 * Internal Dependencies
 */
import IconShield from '@/svg/icon-shield.svg';
import IconOptions from '@/svg/icon-options.svg';
import IconVideo from '@/svg/icon-video.svg';
import IconPeople from '@/svg/icon-people.svg';
import IconPadlock from '@/svg/icon-padlock.svg';
import IconMessage from '@/svg/icon-message.svg';
import IconPost from '@/svg/icon-post.svg';
import IconDollar from '@/svg/icon-dollar.svg';
import IconPlay from '@/svg/icon-play.svg';
import IconLighting from '@/svg/icon-lighting.svg';

const defaultIcons = [
    {
        icon: <IconShield width={18} />,
        text: <Trans>Guaranteed Instant Delivery</Trans>
    },
    {
        icon: <IconOptions />,
        text: <div><span><Trans>Option to</Trans></span> <Trans> split likes</Trans></div>
    },
    {
        icon: <IconVideo />,
        text: <Trans>Includes video views</Trans>
    },
    {
        icon: <IconPeople />,
        text: <Trans>REAL engagement from REAL people</Trans>
    },
    {
        icon: <IconPadlock />,
        text: <Trans>No password required</Trans>
    },
    {
        icon: <IconMessage />,
        text: <div><span>24/7</span> <Trans>Live Support</Trans></div>
    },

];

const automaticIcons = [
    {
        icon: <IconPost />,
        text: <Trans>Up to 4 posts per day</Trans>
    },
    {
        icon: <IconVideo />,
        text: <div><span><Trans>Free views</Trans></span> <Trans> on all videos</Trans></div>
    },
    {
        icon: <IconPeople />,
        text: <Trans>REAL likes from REAL people</Trans>
    },
    {
        icon: <IconPadlock />,
        text: <Trans>Always Safe and Secure</Trans>
    },
    {
        icon: <IconMessage />,
        text: <div><span>24/7</span> <Trans> Customer Support</Trans></div>
    },
    {
        icon: <IconDollar />,
        text: <Trans>30-Day Money Back Guarantee</Trans>
    },

];

const likesIcons = [
    {
        icon: <IconLighting />,
        text: <Trans>Guaranteed Instant Delivery</Trans>
    },
    {
        icon: <IconOptions />,
        text: <div><span><Trans>Option to </Trans></span> <Trans> split likes</Trans></div>
    },
    {
        icon: <IconVideo />,
        text: <Trans>Includes video views</Trans>
    },
    {
        icon: <IconPeople />,
        text: <Trans>REAL engagement from REAL people</Trans>
    },
    {
        icon: <IconPadlock />,
        text: <Trans>No password required</Trans>
    },
    {
        icon: <IconMessage />,
        text: <div><span>24/7</span> <Trans> Live Support</Trans></div>
    },

];

const followersIcons = [
    {
        icon: <IconLighting />,
        text: <Trans>High Quality Followers</Trans>
    },
    {
        icon: <IconMessage />,
        text: <div><span>24/7</span> <Trans> Live Support</Trans></div>
    },
    {
        icon: <IconVideo />,
        text: <Trans>Includes video views</Trans>
    },
    {
        icon: <IconPeople />,
        text: <Trans>Real Active Followers</Trans>
    },
    {
        icon: <IconPadlock />,
        text: <Trans>No password required</Trans>
    },

];

const viewsIcons = [
    {
        icon: <IconPlay />,
        text: <Trans>Order start in 60 seconds</Trans>
    },
    {
        icon: <IconPeople />,
        text: <Trans>High quality views</Trans>
    },
    {
        icon: <IconPadlock />,
        text: <Trans>No password required</Trans>
    },
    {
        icon: <IconLighting />,
        text: <div><Trans>Fast Delivery </Trans> <span> (5 <Trans> minutes</Trans>)</span></div>
    },
    {
        icon: <IconMessage />,
        text: <div><span>24/7</span> <Trans>Live Support</Trans></div>
    },

];

export {
    defaultIcons,
    automaticIcons,
    likesIcons,
    followersIcons,
    viewsIcons
}
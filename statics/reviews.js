export const ReviewsInstagramSchema = {
  '@context': 'https://schema.org/',
  '@type': 'Product',
  name: 'Buzzoid IG Likes',
  image:
    'https://buzzoid.com/wp-content/themes/buzz/v2/assets/images/buy-instagram-likes.png',
  url: 'https://buzzoid.com/buy-instagram-likes/',
  description:
    'A slight boost to help you and your Instagram post perform better.',
  sku: 16,
  mpn: 16,
  brand: { '@type': 'Brand', name: 'Buzzoid' },
  aggregateRating: {
    '@type': 'AggregateRating',
    ratingValue: '5',
    ratingCount: '97',
    bestRating: 5,
    worstRating: 1,
  },
  offers: {
    '@type': 'Offer',
    price: '1.47',
    priceCurrency: 'USD',
    availability: 'http://schema.org/InStock',
    url: 'https://buzzoid.com/buy-instagram-likes/',
    priceValidUntil: '2024-12-01',
  },
  review: [
    {
      '@type': 'Review',
      reviewBody:
        'How did you make verified users with hundreds of thousands of followers to like my picture for such a cheap price? You should increase the prices on premium likes, seriously. I would be willing to pay way more for this service and keep the cheapskates out.',
      datePublished: '2021-09-27T17:34:09+0000',
      author: { '@type': 'Person', name: 'Vlad P' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'They are good and real',
      datePublished: '2021-01-11T05:58:43+0000',
      author: { '@type': 'Person', name: 'sagebonque' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Actually worked and very professional.',
      datePublished: '2021-01-09T20:59:37+0000',
      author: { '@type': 'Person', name: 'Harry' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Awesome app and highly recommend',
      datePublished: '2021-01-01T21:19:05+0000',
      author: { '@type': 'Person', name: 'Jose Fernandez' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Highly recommend this platform.',
      datePublished: '2020-12-26T10:28:57+0000',
      author: { '@type': 'Person', name: 'Tina' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: "Great website all are instant. Don't miss this",
      datePublished: '2020-12-25T11:32:02+0000',
      author: { '@type': 'Person', name: 'Anandhu' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'It works so well thank you so much !! Will order again !',
      datePublished: '2020-12-22T18:33:07+0000',
      author: { '@type': 'Person', name: 'Dan' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Very good app and trusted',
      datePublished: '2020-12-18T14:25:34+0000',
      author: { '@type': 'Person', name: 'Akshat' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Best app ever...it really gives you real likes',
      datePublished: '2020-12-15T05:36:09+0000',
      author: { '@type': 'Person', name: 'Chimne lastkid' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: "It's a better way to increase your likes",
      datePublished: '2020-12-14T19:55:52+0000',
      author: { '@type': 'Person', name: 'Vaibhav Donerao' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Amazing an so helpful\u2764\ufe0f',
      datePublished: '2020-12-12T09:02:55+0000',
      author: { '@type': 'Person', name: 'Tony' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Great value for such a small fee. If you use Instagram, this is really worth giving a go',
      datePublished: '2018-10-30T15:19:45+0000',
      author: { '@type': 'Person', name: 'Martha King' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I use this service for 3 of my Instagram social media accounts and I have seen significant growth over the past few months. Will continue to use forever!!!',
      datePublished: '2018-09-13T16:44:39+0000',
      author: { '@type': 'Person', name: 'Frase Chancis' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'The likes on my Instagram account went up in seconds. I even got new followers according to my metrics',
      datePublished: '2018-07-23T19:35:54+0000',
      author: { '@type': 'Person', name: 'Vadim' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'This site has made the impossible possible. My likes skyrocketed within minutes. Check it out surely!',
      datePublished: '2018-03-23T01:39:20+0000',
      author: { '@type': 'Person', name: 'Peter' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Brilliant outcomes, despite spending little cash. Likes have filled my posts!',
      datePublished: '2018-03-23T01:38:55+0000',
      author: { '@type': 'Person', name: 'Kathy G McDonnell' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Never felt so excited about my posts. With Buzzoid Instagram likes can be attained quick and easy!',
      datePublished: '2018-03-23T01:38:15+0000',
      author: { '@type': 'Person', name: 'Kelly' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Loved, loved, and loved what I got! Likes filled my posts. You have to try it to believe it.',
      datePublished: '2018-03-23T01:37:35+0000',
      author: { '@type': 'Person', name: 'Charles' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I blog and needed to reach a wider audience. Took up this option and am satisfied beyond words.',
      datePublished: '2018-03-23T01:37:11+0000',
      author: { '@type': 'Person', name: 'Adrian' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Being an Instagram model I needed likes. I tried this out and received successful results!',
      datePublished: '2018-03-23T01:36:40+0000',
      author: { '@type': 'Person', name: 'Ruby' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Checked it out as a joke, turned out to work wonders. Definitely recommending it to all!',
      datePublished: '2018-03-23T01:35:25+0000',
      author: { '@type': 'Person', name: 'Edward' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'All I wanted was more likes. With Buzzoid getting unlimited likes in minutes is possible.',
      datePublished: '2018-03-23T01:34:44+0000',
      author: { '@type': 'Person', name: 'Joseph' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'It was recommended to me. Likes began trickling in within minutes. Buzz is the best.',
      datePublished: '2018-03-23T01:34:17+0000',
      author: { '@type': 'Person', name: 'Mark Guimond' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Needed to increase my likes in a short time. Choose this option and I am so happy.',
      datePublished: '2018-03-23T01:32:34+0000',
      author: { '@type': 'Person', name: 'Sarah Q Andrew' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Visiting for the first time. Quick likes, speedy and worth-the-cash services is what I received.',
      datePublished: '2018-03-23T01:31:42+0000',
      author: { '@type': 'Person', name: 'Michael Manley' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I needed to get more likes and now I have found it in at reasonable rates!',
      datePublished: '2018-03-23T01:31:08+0000',
      author: { '@type': 'Person', name: 'Thomas' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Thought it was a scam, but I will be back. It was so easy to get likes.',
      datePublished: '2018-03-23T01:30:41+0000',
      author: { '@type': 'Person', name: 'Angelo Gately' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Buzzoid has me spellbound. It provides such radical results at extremely cheap rates are unbelievable.',
      datePublished: '2018-03-23T01:30:08+0000',
      author: { '@type': 'Person', name: 'Cecil' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Started out as an experiment to get more likes on my page. I am addicted now.',
      datePublished: '2018-03-23T01:29:33+0000',
      author: { '@type': 'Person', name: 'Brenda' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I reached out for unlimited likes in a short time! I am so happy with results!',
      datePublished: '2018-03-23T01:28:57+0000',
      author: { '@type': 'Person', name: 'Scott' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Getting the ideal number of desired likes has never been simpler. Going to try it again!',
      datePublished: '2018-03-23T01:28:20+0000',
      author: { '@type': 'Person', name: 'Kevin' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Always delivering what is promised. Buzzoid is a name that can be trusted by you.',
      datePublished: '2018-03-23T01:27:45+0000',
      author: { '@type': 'Person', name: 'Samrat' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'The one and only choice to buy likes. Amazed by the swift delivery.',
      datePublished: '2018-03-23T01:26:21+0000',
      author: { '@type': 'Person', name: 'Ali' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'My first ever experience of buying Instagram likes and a great experience. Thanks, Buzzoid!',
      datePublished: '2018-03-23T01:25:53+0000',
      author: { '@type': 'Person', name: 'V' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'OMG! Absolutely stunned when the likes got delivered so quickly. I\u2019m coming back more!',
      datePublished: '2018-03-23T01:25:21+0000',
      author: { '@type': 'Person', name: 'Steph' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'The perfect option to buy instagram likes. Bid goodbye to likes from fake users.',
      datePublished: '2018-03-23T01:24:56+0000',
      author: { '@type': 'Person', name: 'Calron' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Trusted service and prompt delivery of IG likes from real users. No complaints whatsoever!',
      datePublished: '2018-03-23T01:24:33+0000',
      author: { '@type': 'Person', name: 'Anisha M' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Coolest service ever. Buy likes on Insta without burning a hole in your pocket.',
      datePublished: '2018-03-23T01:23:54+0000',
      author: { '@type': 'Person', name: 'Rex' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Received 100% authentic likes within a blink of an eye. See it for yourself.',
      datePublished: '2018-03-23T01:23:11+0000',
      author: { '@type': 'Person', name: 'Sujan' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Bought affordable ig likes from Buzzoid and I am not disappointed. Try it out!',
      datePublished: '2018-03-23T01:22:33+0000',
      author: { '@type': 'Person', name: 'Diar' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I decided to buy instagram likes and the request was processed in seconds. Satisfied!',
      datePublished: '2018-03-23T01:21:58+0000',
      author: { '@type': 'Person', name: 'Moshan' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Ordered for Instagram likes in Buzzoid and received them in minutes. Incredibly fast services.',
      datePublished: '2018-03-23T01:21:24+0000',
      author: { '@type': 'Person', name: 'Patel' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'The quality likes from the house of Buzzoid has kept me spellbound. Thanks!',
      datePublished: '2018-03-23T01:20:56+0000',
      author: { '@type': 'Person', name: 'Aniston' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'The quick delivery was not expected. But, to my surprise Buzz has come up with an extraordinary service.',
      datePublished: '2018-03-23T01:20:21+0000',
      author: { '@type': 'Person', name: 'Habib' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I have bought instagram likes from Buzzoid.com and their quick delivery has made the experience really great!',
      datePublished: '2018-03-23T01:19:45+0000',
      author: { '@type': 'Person', name: 'Harry' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Quick delivery of 1000 likes and easy payment has really left me mesmerized. Keep it up guys!',
      datePublished: '2018-03-23T01:19:05+0000',
      author: { '@type': 'Person', name: 'Jack' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Hey Guys! It\u2019s pretty amazing the way you offer the service. These likes works wonders on my business!',
      datePublished: '2018-03-23T01:18:02+0000',
      author: { '@type': 'Person', name: 'Joshua' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Was not sure whether to use the service, but invested $40 to buy REAL likes and seriously I got complete value of money. Thanks guys!',
      datePublished: '2018-03-23T01:17:32+0000',
      author: { '@type': 'Person', name: 'Henry' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'With lots of doubt in mind, I have tried the Buzzoid service and now it has become simply irresistible. Love you guys!',
      datePublished: '2018-03-23T01:16:54+0000',
      author: { '@type': 'Person', name: 'Ashton Freeman' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'The jaw-dropping service from these guys has really helped my business to grow. I bought 500 likes which turned into 1500 (I think I hit the explore page) within few days.',
      datePublished: '2018-03-23T01:15:47+0000',
      author: { '@type': 'Person', name: 'Ashley' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Buzzoid I can\u2019t thank you enough for what you have done for me. My business is doing wonders with your likes. Thanks a ton!',
      datePublished: '2018-03-23T01:14:43+0000',
      author: { '@type': 'Person', name: 'Sydney' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'How can anyone offer such cheap price? I\u2019m not complaining! Thanks guys.',
      datePublished: '2018-03-23T01:12:59+0000',
      author: { '@type': 'Person', name: 'Jac' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'No bots only real likes! So, I am very happy with the service of Buzzoid. Thanks a lot!',
      datePublished: '2018-03-23T01:11:19+0000',
      author: { '@type': 'Person', name: 'Anisha' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Wow, this service is so quick and fast! Guys believe me there is no need to compromise the quality. The likes are 100% real.',
      datePublished: '2018-03-23T01:09:54+0000',
      author: { '@type': 'Person', name: 'James' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Was really doubtful about getting real likes, but Buzzoid has made everything so simple and easy. I can\u2019t thank them enough!',
      datePublished: '2018-03-23T01:08:55+0000',
      author: { '@type': 'Person', name: 'Emily Walter' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'It has only been possible for Buzzoid to get real likes. Didn\u2019t expect something like this would happen and I will be enjoying the top position.',
      datePublished: '2018-03-23T01:08:02+0000',
      author: { '@type': 'Person', name: 'Robert Elms' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Leave the rest. Here is the best!',
      datePublished: '2018-03-23T00:37:00+0000',
      author: { '@type': 'Person', name: 'Nelson' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'This site is pretty good in providing real-time followers instantly. Reliable enough I can say!',
      datePublished: '2018-03-23T00:36:38+0000',
      author: { '@type': 'Person', name: 'Amy' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Do not get tricked by other website like I did. This is simply the right one just trust me!',
      datePublished: '2018-03-23T00:36:09+0000',
      author: { '@type': 'Person', name: 'Oliver' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'This is the third time I am making a purchase with them and yeah I can\u2019t get enough of their services!',
      datePublished: '2018-03-23T00:35:43+0000',
      author: { '@type': 'Person', name: 'Nick' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "If you don't wanna get hustled, just go for buzzoid.com for buying actual instagram followers!",
      datePublished: '2018-03-23T00:35:16+0000',
      author: { '@type': 'Person', name: 'Duke' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Can it get any easier than this? Just click on a link and unlimited number of followers! Superb!',
      datePublished: '2018-03-23T00:34:36+0000',
      author: { '@type': 'Person', name: 'Damien' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Best is the word for them! I don\u2019t know how to put it but simply they are the greatest I have seen by now.',
      datePublished: '2018-03-23T00:34:11+0000',
      author: { '@type': 'Person', name: 'Chris' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Want to gain real time followers at your convenience? Grab then right now!',
      datePublished: '2018-03-23T00:33:45+0000',
      author: { '@type': 'Person', name: 'Aisha' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'No doubt in ranking it the best! Please someone go give them a crown they deserve it!',
      datePublished: '2018-03-23T00:30:51+0000',
      author: { '@type': 'Person', name: 'Karl' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I have browsed through quite a number of website and finally settled down on this one as it is offering everything I am looking for!',
      datePublished: '2018-03-23T00:30:02+0000',
      author: { '@type': 'Person', name: 'Katie' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Let me know if you guys have any other suggestions for buying instagram followers. For now I am more than happy with them!',
      datePublished: '2018-03-23T00:29:32+0000',
      author: { '@type': 'Person', name: 'Josh' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'It totally worked for me and I gained a huge number of followers! Just wanted to share in case you guys are considering them!',
      datePublished: '2018-03-23T00:28:59+0000',
      author: { '@type': 'Person', name: 'Daniel' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Five stars from me for their brilliance!',
      datePublished: '2018-03-23T00:28:32+0000',
      author: { '@type': 'Person', name: 'Elizabeth' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I gained a lot number of followers without even following them back. Isn\u2019t that wonderful?',
      datePublished: '2018-03-23T00:28:05+0000',
      author: { '@type': 'Person', name: 'Sarah' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Right from the moment I bought followers from this site, I am gaining unbelievably large number of followers every day. Amazing indeed!',
      datePublished: '2018-03-23T00:27:36+0000',
      author: { '@type': 'Person', name: 'Justin' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Whoa I am thrilled to announce how much I am pleased to enjoy huge number of followers for the absolutely little amount of price I paid.',
      datePublished: '2018-03-23T00:27:06+0000',
      author: { '@type': 'Person', name: 'Jennifer' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'It\u2019s easy-breezy and simply great! Go for it guys you won\u2019t be disappointed!',
      datePublished: '2018-03-23T00:26:23+0000',
      author: { '@type': 'Person', name: 'Jack' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I can\u2019t believe this site is actually providing real time followers at such an affordable rate. Highly recommended!',
      datePublished: '2018-03-23T00:25:55+0000',
      author: { '@type': 'Person', name: 'Raven' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'This website works like magic in delivering instant followers after purchasing them.',
      datePublished: '2018-03-23T00:25:24+0000',
      author: { '@type': 'Person', name: 'Isaiah' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Guys give it a shot! They are totally worthwhile for delivering real-time followers.',
      datePublished: '2018-03-23T00:23:20+0000',
      author: { '@type': 'Person', name: 'Freddy' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I was so skeptical to try anything like this but it was only $2 so what the hell. And instantly after I paid I got the likers it was so tight! I think a lot of people do this, more then you think!',
      datePublished: '2017-12-30T14:31:55+0000',
      author: { '@type': 'Person', name: 'Nicole' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'IT WORKS!',
      datePublished: '2017-12-30T06:47:09+0000',
      author: { '@type': 'Person', name: 'Boi' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Fast and reliable and quick with delivery. 100% real Deal',
      datePublished: '2017-12-20T15:28:27+0000',
      author: { '@type': 'Person', name: 'Pele' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "I want to buy likes for a competition but I don't have access to the competition page",
      datePublished: '2017-11-14T23:26:57+0000',
      author: { '@type': 'Person', name: 'Jay' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'The likes were real! Not like the other cheap ones where all the people have 0 post and 0 followers these actully are notmal people with normal likes',
      datePublished: '2017-09-29T18:12:51+0000',
      author: { '@type': 'Person', name: 'Vlad' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Immediately received my likes and you can split them into your pictures. The likes are from real followers!',
      datePublished: '2017-08-30T12:10:23+0000',
      author: { '@type': 'Person', name: 'Rmna' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'THX MY BROS',
      datePublished: '2017-08-22T16:06:11+0000',
      author: { '@type': 'Person', name: 'Icookforfun30' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "Simply great ! Was sceptical but I'm glad that didn't stop me",
      datePublished: '2017-07-21T23:29:55+0000',
      author: { '@type': 'Person', name: 'Jess' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I was really amazed by the quality of likes and the speed of the service.',
      datePublished: '2017-07-21T18:47:15+0000',
      author: { '@type': 'Person', name: 'Alex' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'i bought service and deliver asap thanks great experience. Fast delivery',
      datePublished: '2017-06-18T01:28:48+0000',
      author: { '@type': 'Person', name: 'habib ullah' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'i bought service and deliver asap thanks great experience. Fast delivery',
      datePublished: '2017-06-18T01:09:32+0000',
      author: { '@type': 'Person', name: 'habib ullah' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Worked as advertised and very quick delivery within seconds of completing the payment. Keep it up guys!',
      datePublished: '2017-05-08T19:32:02+0000',
      author: { '@type': 'Person', name: 'Josh' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'It is legit and does actually work... And as keemstar would say: " Fast AF Boiii"',
      datePublished: '2017-04-29T07:16:32+0000',
      author: { '@type': 'Person', name: 'Irshaad OOZEER' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Quick, Fast & Easy. The pricing is perfect and I was very surprised that I received followers so quickly. The followers I bought, immediately had an impact on my social media presence. I had new purchases from new customers in the same day.',
      datePublished: '2017-04-20T23:16:01+0000',
      author: { '@type': 'Person', name: 'Love' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I love the service because its helping me reach the "Top Posts" on certain hashtags giving me a lot of organic engagement, the option to choose the time delivery is also great and the low price is great also!',
      datePublished: '2017-04-18T23:14:55+0000',
      author: { '@type': 'Person', name: 'YoungEntrepreneur01' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: "Really fast!! I didn't expect that. Thank you.",
      datePublished: '2017-03-26T15:23:54+0000',
      author: { '@type': 'Person', name: 'Eldin' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Sound good to me! 10,000 likes and followers. I must try it.',
      datePublished: '2017-02-19T19:45:47+0000',
      author: { '@type': 'Person', name: 'Mohammad' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Amazed by the sheer speed and authenticity.would have loved 1500 likes being included in the package',
      datePublished: '2017-01-25T14:32:19+0000',
      author: { '@type': 'Person', name: 'Larry' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'WOW! I am just amazed how fast and cheap those likes are. unlike other providers, these are REAL too!!!',
      datePublished: '2016-12-22T10:08:12+0000',
      author: { '@type': 'Person', name: 'Sindey' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Holy crap, this is so FAST! Also, if you guys care about quality, the likes from Buzzoid is actually 100% real people and not bots.',
      datePublished: '2016-12-22T10:04:54+0000',
      author: { '@type': 'Person', name: 'Jens' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Did not expect to receive real likes, because I\'ve tried all those other services and they did deliver, however it was fake likes which did not count for me who is trying to reach the "Top Posts" on certain hashtags. Buzzoid actually got me straight to the top and gave me tons of organic real followers because of that!',
      datePublished: '2016-12-20T20:26:39+0000',
      author: { '@type': 'Person', name: 'James' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
  ],
};

export const ReviewsFollowersSchema = {
  '@context': 'https://schema.org/',
  '@type': 'Product',
  name: 'Buzzoid IG Followers',
  image:
    'https://buzzoid.com/wp-content/themes/buzz/v2/assets/images/buy-instagram-followers.png',
  url: 'https://buzzoid.com/buy-instagram-followers/',
  description:
    'A slight follow boost to help your Instagram account skyrocket.',
  sku: 18,
  mpn: 18,
  brand: { '@type': 'Brand', name: 'Buzzoid' },
  aggregateRating: {
    '@type': 'AggregateRating',
    ratingValue: '5',
    ratingCount: '133',
    bestRating: 5,
    worstRating: 1,
  },
  offers: {
    '@type': 'Offer',
    price: '2.97',
    priceCurrency: 'USD',
    availability: 'http://schema.org/InStock',
    url: 'https://buzzoid.com/buy-instagram-followers/',
    priceValidUntil: '2024-12-01',
  },
  review: [
    {
      '@type': 'Review',
      reviewBody: 'Love this app!',
      datePublished: '2021-01-10T18:31:22+0000',
      author: { '@type': 'Person', name: 'Ellie' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'better than all other websites and this one actually works!',
      datePublished: '2021-01-08T04:12:00+0000',
      author: { '@type': 'Person', name: 'andre' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Great service and like that you\u2019re given a trial before paying!',
      datePublished: '2021-01-05T14:35:46+0000',
      author: { '@type': 'Person', name: 'Kay' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'It actually works and all of them were legit !!',
      datePublished: '2021-01-03T20:17:33+0000',
      author: { '@type': 'Person', name: 'Jason' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Really quick and easy',
      datePublished: '2020-12-24T19:20:37+0000',
      author: { '@type': 'Person', name: 'Bailey' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Amazing, so helpful and has helped me improve my page and earn more followers with the positive engagement increase.',
      datePublished: '2020-12-24T08:58:38+0000',
      author: { '@type': 'Person', name: 'Matt Cromer' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Great way to get followers',
      datePublished: '2020-12-14T06:22:30+0000',
      author: { '@type': 'Person', name: 'Kamerin carter' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Amazing service and I love how it increases your attention',
      datePublished: '2020-12-02T19:14:56+0000',
      author: { '@type': 'Person', name: 'Ricky' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'I like the way the followers come in just 30 minutes',
      datePublished: '2019-06-02T03:53:38+0000',
      author: { '@type': 'Person', name: 'Marco Menjivar' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'It worked thanks a lot',
      datePublished: '2019-05-28T02:08:28+0000',
      author: { '@type': 'Person', name: 'Naila' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I won\u2019t lie, I was really skeptical at first but my heart got filled with excitement as soon as my real followers starting filling up my notifications. I am extremely satisfied!',
      datePublished: '2019-05-27T04:33:43+0000',
      author: { '@type': 'Person', name: 'Nthabi' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Respecto',
      datePublished: '2019-05-17T00:56:18+0000',
      author: { '@type': 'Person', name: 'Doggblitz4' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I was a bit skeptical at first, but the followers started coming INSTANTLY after i bought them. I suggest you all use this if you want some followers.',
      datePublished: '2018-12-10T02:35:43+0000',
      author: { '@type': 'Person', name: 'Nathan Rice' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I will give them 5 Stars. It started to go up really fast and then went down a bit and then started going up again! Thank you so much\u2019 I\u2019m Really considering the 40 dollars now!',
      datePublished: '2018-12-06T00:54:38+0000',
      author: { '@type': 'Person', name: 'Kalah' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'It works great fam, I bought 200 followers and 100 likes and they were delivered within I don\u2019t know about 3-5 minutes . You guys can dm me on Instagram @fbn5k to see proof . I\u2019ll send you the screenshots of the emails I got that confirmed my purchases. Also follow me on Snapchat @hooper.saun. Thanks that\u2019s all.',
      datePublished: '2018-11-28T05:31:40+0000',
      author: { '@type': 'Person', name: 'Rip Stan Lee' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Thank you so much Buzzoid. I sugget to buy followers on Buzzoid very fast real followers. Thanks again',
      datePublished: '2018-11-25T19:19:34+0000',
      author: { '@type': 'Person', name: 'Besnik' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Great tool to effectively grow your following in a compliant way.',
      datePublished: '2018-11-22T13:40:06+0000',
      author: { '@type': 'Person', name: 'Badol' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Super fast service, simply the best quality followers!',
      datePublished: '2018-11-21T17:45:26+0000',
      author: { '@type': 'Person', name: 'Bernabedelvillar' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Great service, great support. 5 Stars from me.',
      datePublished: '2018-11-18T11:08:21+0000',
      author: { '@type': 'Person', name: 'Frank Anderson' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Really helpful!',
      datePublished: '2018-10-25T14:45:14+0000',
      author: { '@type': 'Person', name: 'Heather Bond' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I would like to thank Buzzoid for their professional follow up about their services.',
      datePublished: '2018-10-08T14:46:10+0000',
      author: { '@type': 'Person', name: 'Lara' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'I love this sote. 100% REAL FOLLOWERS!',
      datePublished: '2018-10-07T23:32:48+0000',
      author: { '@type': 'Person', name: 'Kiara' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'This website worked! Apart from the fact that that I started with 50 followers, bought the 500 followers package and ended up with 498 followers. Maybe make sure that people don\u2019t unfollow. Thanks - Franky.',
      datePublished: '2018-10-05T06:21:28+0000',
      author: { '@type': 'Person', name: 'Franky Martin' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Works in a couple hours',
      datePublished: '2018-09-27T21:14:08+0000',
      author: { '@type': 'Person', name: 'Victor M Mares' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I am amazed at how fast and easy this was. This is definitely going to help my business and I am so grateful!',
      datePublished: '2018-09-20T02:37:09+0000',
      author: { '@type': 'Person', name: 'Brittney' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Wow what a service! Talk about delivering...over the top service!!! Would I recommend...a 1,000 times YES!',
      datePublished: '2018-09-19T13:03:33+0000',
      author: { '@type': 'Person', name: 'John Davis' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'five star',
      datePublished: '2018-09-07T16:58:42+0000',
      author: { '@type': 'Person', name: 'igacartier' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'good',
      datePublished: '2018-08-31T15:58:13+0000',
      author: { '@type': 'Person', name: 'Tahir Salih' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'in less than an hour i had my 750 followers. rapid delivery as promised! i will definitively will continue working with Buzzoid. I recommend their services! they are professionals in this matter.',
      datePublished: '2018-08-17T23:18:35+0000',
      author: { '@type': 'Person', name: 'Steven Wood' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Worth it! I was nervous at first because the idea seemed sketchy to me but I made the purchase and not even a minute later my phone was lighting up with so many notifications. I had my followers instantly! And they don\u2019t look like fake followers either! Great great service, good company! 10/10 would recommend.',
      datePublished: '2018-08-06T09:43:21+0000',
      author: { '@type': 'Person', name: 'DJ' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Dear, I have used your service 4 or 5 times so far. I am a loyal customer! would you please do a huge favor and give me 50.000 Followers for $150 ? I will pay today! and continue using your service forever, and refer you to my friends! Please let me know, as I am looking to buy 50k followers asap!',
      datePublished: '2018-08-06T05:37:42+0000',
      author: { '@type': 'Person', name: 'Eayd Fairaq' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Best service for instant IG followers! Will keep using!',
      datePublished: '2018-08-03T13:02:32+0000',
      author: { '@type': 'Person', name: 'John Rumberger' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Awesome service! Followers keep refilling! Gonna keep using Buzzoid!',
      datePublished: '2018-08-02T10:52:23+0000',
      author: { '@type': 'Person', name: 'John Rumberger' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Worked. Had money left over on Visa card and needed a boost so just spent the rest on this.',
      datePublished: '2018-07-26T21:07:03+0000',
      author: { '@type': 'Person', name: 'Billy James' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Amazing works perfect and on time',
      datePublished: '2018-07-26T19:16:17+0000',
      author: { '@type': 'Person', name: '@ultimate_human_freedom' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "Really efficient service, they've really helped me improve my company V21's public reach!",
      datePublished: '2018-07-10T00:14:08+0000',
      author: { '@type': 'Person', name: 'Jack Gaisford' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Great customer service. Fast response time. On the weekend it was around 6-8 hours but during the week 10-15 min response time. Incredible! I bought the 500 followers and was sadly unsatisfied and received a full refund!!! Fast and great customer service and would highly recommend to someone who just wants their numbers to look higher without actually having real followers. All in all a good company even tho I was very skeptical of all the negative reviews from other websites. But who really leaves reviews besides people who had a bad experience anyway. I\u2019m leaving this one to hopefully show this is in fact a great company. Just didn\u2019t work out for me.',
      datePublished: '2018-06-06T21:03:59+0000',
      author: { '@type': 'Person', name: 'Matt' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Instant delivery! Got 100 followers within 3 or less minutes.',
      datePublished: '2018-05-28T01:51:07+0000',
      author: { '@type': 'Person', name: 'Karen Tatum' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'good',
      datePublished: '2018-05-25T18:15:43+0000',
      author: { '@type': 'Person', name: 'sam' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I\u2019ve tried another service and my followers never came through. Can I get a response to ensure this site is real and will work?',
      datePublished: '2018-05-17T20:23:08+0000',
      author: { '@type': 'Person', name: 'Savannah' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "I don't usually write reviews. I was skeptical at first too thought it was a scam, but it worked! Will definitely use this from now on :) Paying for a min of 100 likes was worth it!",
      datePublished: '2018-05-12T06:02:31+0000',
      author: { '@type': 'Person', name: 'Adrbm' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Didn\u2019t get the followers instantly but The customer support is so fast! Totally worth!',
      datePublished: '2018-05-07T21:08:29+0000',
      author: { '@type': 'Person', name: 'James' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "I purchased 500 likes as Instagram kept flagging my new picture as 'too many hashtags' even though i was yet to tag. Knowing I'd be losing engagement I purchase the likes to keep my post relevant. The likes came within 3minutes! I was very impressed as it was my first time. Will use again x",
      datePublished: '2018-05-07T10:01:33+0000',
      author: { '@type': 'Person', name: 'Kat' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "I had all of my followers within an hour. They came in chunks, which worried me at first, but this service works and it's super fast!",
      datePublished: '2018-04-09T00:58:46+0000',
      author: { '@type': 'Person', name: 'caro' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Quick, Efficient, Worth-the-money and Guaranteed Results! Will I be back for more? I think this review answers that question',
      datePublished: '2018-03-23T21:46:04+0000',
      author: { '@type': 'Person', name: 'Freddy Smith' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Made payment at night, woke up with three times the followers! Is it a dream? No, just Buzzoid! Hahaha, thanks again gentlemen. Top service you guys have here.',
      datePublished: '2018-03-23T21:45:34+0000',
      author: { '@type': 'Person', name: 'Jack' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Finally, a site that serves its clients heartily. So happy with results, will be back for more soon.',
      datePublished: '2018-03-23T21:45:08+0000',
      author: { '@type': 'Person', name: 'Ajay' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Never did I think it possible to boost follower numbers so quickly. Buzzoid is a magician!',
      datePublished: '2018-03-23T21:44:30+0000',
      author: { '@type': 'Person', name: 'Paul' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Followers are a status symbol. Increased mine using this site. Worked wonders with no hassles. Thanks, folks!',
      datePublished: '2018-03-23T21:44:05+0000',
      author: { '@type': 'Person', name: 'Colin' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'After an endless search, I finally found a site that gets me followers within minutes! Will be back for more for sure.',
      datePublished: '2018-03-23T21:43:45+0000',
      author: { '@type': 'Person', name: 'Kay' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I was so desperate for followers, just when it seemed undoable; these guys got me what I needed!',
      datePublished: '2018-03-23T21:43:26+0000',
      author: { '@type': 'Person', name: 'Stephen' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'There is something extraordinary about followers increasing in minutes. Buzzoid has me wanting more!',
      datePublished: '2018-03-23T21:43:05+0000',
      author: { '@type': 'Person', name: 'Floyd' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I recently purchased followers and I could not be happier with the results!',
      datePublished: '2018-03-23T21:42:41+0000',
      author: { '@type': 'Person', name: 'iamcardib' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'The purchase of followers has finally helped me to improve my business. It really works pretty well!',
      datePublished: '2018-03-23T21:42:19+0000',
      author: { '@type': 'Person', name: 'daddyyankee' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Honestly wasn\u2019t sure if this was the right choice.. but glad I ordered! Quick delivery, superb support and overall positive vibe.',
      datePublished: '2018-03-23T21:41:59+0000',
      author: { '@type': 'Person', name: 'Kane' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'The totally safe way of acquiring Instagram followers works like a charm every time. Thanks, Buzz!',
      datePublished: '2018-03-23T21:41:15+0000',
      author: { '@type': 'Person', name: 'Karl Singh' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I am now obsessed with the website and its incredible service. Good job Buzz!',
      datePublished: '2018-03-23T21:40:37+0000',
      author: { '@type': 'Person', name: 'Esther' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'The safe payment procedure and easy to buy process makes this an extraordinary solution. Thanks a lot!',
      datePublished: '2018-03-23T21:40:11+0000',
      author: { '@type': 'Person', name: 'James' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'The buying of followers from Buzzoid has really helped me a lot. Seriously, the low charge and high quality have amazed me!',
      datePublished: '2018-03-23T21:39:26+0000',
      author: { '@type': 'Person', name: 'Rose M' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'At first, I was not convinced with the idea. But, soon was really impressed with the service. The followers have really helped my business.',
      datePublished: '2018-03-23T21:39:00+0000',
      author: { '@type': 'Person', name: 'Eddie Thompson' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'A good number of followers can be acquired by just paying a little. I simply love this website! Blows the competition straight out the water.',
      datePublished: '2018-03-23T21:38:31+0000',
      author: { '@type': 'Person', name: 'Julia' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I am really obsessed with the website and its amazing services. Everything was completed literally in a few minutes. LOL. Talk about speed. Cheers fellas!',
      datePublished: '2018-03-23T21:38:03+0000',
      author: { '@type': 'Person', name: 'Wilson Gray' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'The followers really worked for me and I am happy to get such amazing service. Thanks, Buzz!',
      datePublished: '2018-03-23T21:37:44+0000',
      author: { '@type': 'Person', name: 'Diana' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Was looking for IG followers and came across this site. The easy purchase and quick delivery are the USP!',
      datePublished: '2018-03-23T21:37:17+0000',
      author: { '@type': 'Person', name: 'Davidal' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I have got 1000 followers at just a minimal cost. Really amazing! Thanks a lot, Buzz.',
      datePublished: '2018-03-23T21:36:56+0000',
      author: { '@type': 'Person', name: 'lisaalvarez5' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I love this! Because of this website I have been able to buy followers and create my online presence.',
      datePublished: '2018-03-23T21:36:32+0000',
      author: { '@type': 'Person', name: 'Ashley' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "Was not really convinced with the idea. But, I tried it out and received unbelievable results! I'm so grateful for finding yous!",
      datePublished: '2018-03-23T21:36:13+0000',
      author: { '@type': 'Person', name: 'Henry' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Followers to my account have increased a lot and now I am enjoying the popularity. All this has happened because of Buzzoid. Thanks for the easy purchase of followers!',
      datePublished: '2018-03-23T21:35:21+0000',
      author: { '@type': 'Person', name: 'James' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'These guys are doing an incredible job! Within a single day, I have got all my 1000 followers.',
      datePublished: '2018-03-23T21:34:51+0000',
      author: { '@type': 'Person', name: 'dirganovam' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I was really not sure about the followers. But I still took a chance and bought it and was amazed to see the results. Thanks, Buzzoid!',
      datePublished: '2018-03-23T21:34:27+0000',
      author: { '@type': 'Person', name: 'Edward Cook' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'To improve my online presence I have purchased the followers and now I have become quite popular. It works wonderfully!',
      datePublished: '2018-03-23T21:34:06+0000',
      author: { '@type': 'Person', name: 'Steven' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Impatient :) !',
      datePublished: '2018-03-13T17:51:40+0000',
      author: { '@type': 'Person', name: 'Kacem' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'this rating isnt a bot generated review, i thought it was a scam, i had a few bucks so why not try, and it works haha. good to go',
      datePublished: '2018-03-09T00:40:51+0000',
      author: { '@type': 'Person', name: 'Adam' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Love this website a lot',
      datePublished: '2018-03-02T08:49:50+0000',
      author: { '@type': 'Person', name: 'William' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Batia_hasan',
      datePublished: '2018-02-27T19:02:21+0000',
      author: { '@type': 'Person', name: 'Batia_hasan' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Delivered exactly what it said it would in less than an hour. I\u2019m a happy customer',
      datePublished: '2018-02-20T01:58:39+0000',
      author: { '@type': 'Person', name: 'Cc' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "I paid $29.99 for 2500 followers and I haven't received a single new follower. I understand that I may not receive all 2500 but I hope we can get close. Please reply to my e-mail or fulfill my order of 2500 followers. Thank you for your time and help!!",
      datePublished: '2018-02-02T07:26:31+0000',
      author: { '@type': 'Person', name: 'Joe' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Just wanna check they are because of the nearly unbroken 5 star reviews.',
      datePublished: '2018-02-01T03:05:47+0000',
      author: { '@type': 'Person', name: 'testing' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'It worked',
      datePublished: '2018-01-19T20:44:40+0000',
      author: { '@type': 'Person', name: 'Alex' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        '8 Ways to Increase Your Instagram Followers Without Following Back',
      datePublished: '2018-01-14T12:25:12+0000',
      author: { '@type': 'Person', name: 'Aadil' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I am addicted, it is happening instantly but my only question is it possible to choose your audience? eg country ...',
      datePublished: '2017-12-08T22:31:25+0000',
      author: { '@type': 'Person', name: 'Esther' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Paid $10 to buy followers. Took several minutes to work, but this is legit!',
      datePublished: '2017-12-04T13:50:06+0000',
      author: { '@type': 'Person', name: 'Kirk' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "I initially left a 3 star review because after buying 2,500 followers about 200 unfollowed right off the bat. After leaving that review, more followers came in and everything was perfect. It worked as advertised and I've used them again for two of my other businesses. Very happy with the results!",
      datePublished: '2017-11-13T22:39:45+0000',
      author: { '@type': 'Person', name: 'Javier' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I have used their offers and to buy Instagram followers and i like them',
      datePublished: '2017-11-13T05:34:03+0000',
      author: { '@type': 'Person', name: 'Muhammad Junaid' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'We went with the 2500 package and worked within the hour. However, we\u2019ve lost 60 followers so far. Not sure if it\u2019s because IG deleted those accounts or what. So something to lookout for',
      datePublished: '2017-11-09T21:14:04+0000',
      author: { '@type': 'Person', name: 'Jav' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Bought there service countless times .. They always delivered more than ordered. Thumbs up!!',
      datePublished: '2017-10-25T10:18:45+0000',
      author: { '@type': 'Person', name: 'Aamir' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Worked!',
      datePublished: '2017-10-05T05:50:28+0000',
      author: { '@type': 'Person', name: 'Lisa Lee' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I was waaay to skeptical,but I gave it a try .After 5mins I got all my new followers,thanks!!',
      datePublished: '2017-09-26T13:09:22+0000',
      author: { '@type': 'Person', name: 'Lalsa' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Nice service in low rate they give good followers :)',
      datePublished: '2017-09-24T16:48:59+0000',
      author: { '@type': 'Person', name: 'Arvind' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Was skeptical at first, But they delivered right away. Prompt customer service as well.',
      datePublished: '2017-09-22T19:20:42+0000',
      author: { '@type': 'Person', name: 'Angela' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'i love you guys! was satisfied',
      datePublished: '2017-09-22T01:30:25+0000',
      author: { '@type': 'Person', name: 'SlickSocials' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Worked like a charm.',
      datePublished: '2017-09-18T21:08:44+0000',
      author: { '@type': 'Person', name: 'fveitsi' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "Honestly, didn't think it would work. It does. .__. It really works.",
      datePublished: '2017-09-14T22:48:41+0000',
      author: { '@type': 'Person', name: 'Erin' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Not gonna lie , I was sceptical. But it really works! Highly reccomend!',
      datePublished: '2017-09-08T14:29:07+0000',
      author: { '@type': 'Person', name: 'Raeesah Ganie' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I buyed 150 followers and I received instantly I will buy more followers in this site, recommended 100%',
      datePublished: '2017-08-26T00:52:34+0000',
      author: { '@type': 'Person', name: 'Carlos Torres' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Hoping this fantastic service also for FB. Great great JOB!',
      datePublished: '2017-08-23T11:21:57+0000',
      author: { '@type': 'Person', name: 'Andrea' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'The service is great',
      datePublished: '2017-08-20T20:29:37+0000',
      author: { '@type': 'Person', name: 'john' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'In total I bought 100k followers! They ALL came! Awesome tool',
      datePublished: '2017-08-15T10:07:58+0000',
      author: { '@type': 'Person', name: 'Har Stanly' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: "I thought it wassen't gonna work but it did!!!",
      datePublished: '2017-08-12T08:17:45+0000',
      author: { '@type': 'Person', name: 'Ya boy' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Just wow. Giddy!',
      datePublished: '2017-08-08T12:11:26+0000',
      author: { '@type': 'Person', name: 'Krassiva Parker' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "Hi. I was skeptical, so I only bought the smallest amount at first. I am really happy to say that I got my followers! It took about 2.5 hours (it wasn't immediate, so don't freak out if your order isn't either) but I got them!",
      datePublished: '2017-07-28T18:18:15+0000',
      author: { '@type': 'Person', name: 'Julie' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Skeptical at first but the service works!',
      datePublished: '2017-07-13T14:43:50+0000',
      author: { '@type': 'Person', name: 'Andrea' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Works very well. Definitely recommend!',
      datePublished: '2017-07-05T14:59:22+0000',
      author: { '@type': 'Person', name: 'GREATTTT!!!!' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "Wow, I ordered and they delivered ! Didn't even have to wait till the next day. Within the hour I had already received all my followers.",
      datePublished: '2017-06-28T03:40:53+0000',
      author: { '@type': 'Person', name: 'Sharon' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "I was definitely skeptical to how this could work but I received my followers within a few hours. Unfortunately (fortunately) they're real accounts so they can unfollow you however the followers top back up every now and then.",
      datePublished: '2017-06-17T03:36:55+0000',
      author: { '@type': 'Person', name: 'Kayla' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'i was not sure because so many fake websites but seems good and fast they accept paypal and its not too expensive',
      datePublished: '2017-06-15T18:21:11+0000',
      author: { '@type': 'Person', name: 'Angel' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Surprisingly the mishap took place when I attempted to buy followers the second time. My followers were never delivered but they did try to fix the problem and corresponded with me every step of the way. They are now refunding me the 54.99 and promise to solve this dilemma. THANK YOU BUZZOID!',
      datePublished: '2017-06-14T18:42:01+0000',
      author: { '@type': 'Person', name: 'Jacob Hutchins' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'It works I used it 3 times already.',
      datePublished: '2017-05-18T02:24:49+0000',
      author: { '@type': 'Person', name: 'Ashley C' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'This was great! Very quick and easy!',
      datePublished: '2017-05-16T23:41:11+0000',
      author: { '@type': 'Person', name: 'James Murnak' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I was scared thinking it was a scam like most buy following accounts. So I had bought the cheapest to make sure, got the followers within 2 hours and it was actually 15 more than says given. About to buy the $40 package!!',
      datePublished: '2017-04-27T00:46:07+0000',
      author: { '@type': 'Person', name: 'Devin Edwards' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I was scared thinking it was a scam like most buy following accounts. So I had bought the cheapest to make sure, got the followers within 2 hours and it was actually 15 more than says given. About to buy the $40 package!!',
      datePublished: '2017-04-27T00:44:30+0000',
      author: { '@type': 'Person', name: 'Devin Edwards' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "so I've been feeling a little bit depressed lately bc my photos were under appreciated by a lot of people. So I've decided to get a help from Buzzoid and thank god it was not a fucking scam it's real u guys!! v affordable too.",
      datePublished: '2017-04-23T17:27:27+0000',
      author: { '@type': 'Person', name: 'Joe' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: "i was like na, but then now I'm like ya",
      datePublished: '2017-04-23T16:24:01+0000',
      author: { '@type': 'Person', name: 'jim' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: '100% recommend its safe and cheap great service',
      datePublished: '2017-04-22T15:50:48+0000',
      author: { '@type': 'Person', name: 'Josh' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "I can't seem to contact you though the customer service. Could someone email me please",
      datePublished: '2017-04-16T00:14:50+0000',
      author: { '@type': 'Person', name: 'Matt' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Definitely a great service! Thank you!',
      datePublished: '2017-04-15T18:40:24+0000',
      author: { '@type': 'Person', name: 'C' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "This service is pretty dope! Glad I bought from here, and it's cheap too. Clash of Guide",
      datePublished: '2017-04-12T10:52:17+0000',
      author: { '@type': 'Person', name: 'Bright Joe' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'really didnt think it was gonna work, but i was like fuck it its only 10 dollars. then boom it happened',
      datePublished: '2017-03-31T18:27:00+0000',
      author: { '@type': 'Person', name: 'wil' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I used it once and so my follower counter increase....so I bought a few more followers. of the 1100 followers I supposedly purchased my followers increased by 3000. worth it.',
      datePublished: '2017-03-23T13:43:28+0000',
      author: { '@type': 'Person', name: 'W' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'Best',
      datePublished: '2017-03-02T12:47:35+0000',
      author: { '@type': 'Person', name: 'Ricky' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'I am obsessed with this website and their quality! 5/5!',
      datePublished: '2017-02-19T03:26:38+0000',
      author: { '@type': 'Person', name: 'Jovana' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "Like everyone else, I was skeptical. Dropped $7, just to check... that was like, 15 minutes ago. I'm already up 30 followers or so.",
      datePublished: '2017-02-04T17:22:09+0000',
      author: { '@type': 'Person', name: 'Jessica' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "I was really worried about getting into something like this. IM SO GLAD I DECIDED TO GO WITH THIS SITE. Easy, safe (I've used them for a week but so far no identity theft etc.) Just asks for your username which is public anyway. You pay using PAYPAL so that safe too. Obviously nobody wants to out themselves. But after you do it, give em a review so more people can know how awesome this is.",
      datePublished: '2017-02-01T17:17:52+0000',
      author: { '@type': 'Person', name: 'Henry Camacho' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "I was skeptical at first so I spent 7 bucks to test the waters. At first after a hakf hour I only had 5 bew followers so I figured it was bull. Then I realized they trickled in the followers. I got 500 new followers over the course of the next day. It wasn't even remotely suspsious to my friends. They did an absolutely good job. Low key, and quality gaurenttee. Seriously.",
      datePublished: '2017-01-11T22:15:25+0000',
      author: { '@type': 'Person', name: 'Manny' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'It worked for me! I finally purchased after watching a youtube review.',
      datePublished: '2017-01-03T22:28:06+0000',
      author: { '@type': 'Person', name: 'diana' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'We performed a research experiment and found this site to be the BEST in the market. Quick delivery & Easy to use! Thanks Buzzoid!',
      datePublished: '2017-01-02T01:29:55+0000',
      author: { '@type': 'Person', name: 'MIller Scott' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I have bought followers and likes for my instagram account and it works like a charm everytime! Totally safe and way easy to use. Thanks Buzzoid!',
      datePublished: '2016-12-29T00:14:15+0000',
      author: { '@type': 'Person', name: 'cory stricklin' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'pretty good',
      datePublished: '2016-12-24T00:00:00+0000',
      author: { '@type': 'Person', name: 'euwjhu' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'Payed 40 dollars for 5000 fans and got them extremely fast. EACH SINGLE ONE FOLLOWER. I love this website!',
      datePublished: '2016-12-21T10:24:44+0000',
      author: { '@type': 'Person', name: 'Alex' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        "Based on other people's reviews, I was also skeptical. Tried it out, took a few minutes and got it all! Thanks Buzz!",
      datePublished: '2016-12-20T20:27:59+0000',
      author: { '@type': 'Person', name: 'James' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'The followers I purchased accelerated my social media marketing efforts. More people follow my account because it looks more popular.',
      datePublished: '2016-12-20T19:25:53+0000',
      author: { '@type': 'Person', name: 'Harry Murphy' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody:
        'I was a skeptical at first but the service provided was great.',
      datePublished: '2016-12-20T19:17:45+0000',
      author: { '@type': 'Person', name: 'Amy Perez' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
    {
      '@type': 'Review',
      reviewBody: 'great!',
      datePublished: '2016-12-20T19:14:40+0000',
      author: { '@type': 'Person', name: 'Johnny C.' },
      reviewRating: {
        '@type': 'Rating',
        ratingValue: 5,
        bestRating: 5,
        worstRating: 1,
      },
    },
  ],
};

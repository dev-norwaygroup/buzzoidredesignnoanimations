module.exports = {
  reactStrictMode: true,
  i18n: {
    locales: ['en', 'fr', 'pseudo'],
    defaultLocale: 'en',
  },
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack'],
    });

    return config;
  },
  env: {
    BASE_URL: process.env.BASE_URL,
  }
};

import { useQuery } from "react-query";
import http from '../http';

const usePostsCategoryShow = (category, config) => {
    const select = (response) => response;
    const onError = (error) => console.log(error);

    let url = `/posts?_embed&categories=${category}`;

    if (config.postsPerPage) {
        url += `&per_page=${config.postsPerPage}`;
    }

    if (config.page) {
        url += `&page=${config.page}`;
    }

    return useQuery(
        ['posts', category, config.page],
        () => http.get(url),
        {
            select,
            onError
        }
    )
}

export default usePostsCategoryShow;

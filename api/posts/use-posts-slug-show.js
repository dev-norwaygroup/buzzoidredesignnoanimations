import { useQuery } from "react-query";
import http from '../http';

const usePostsSlugShow = (slug) => {
    const select = (response) => response.data;
    const onError = (error) => console.log(error);
    
    return useQuery(
        ['posts', slug],
        () => http.get(`/posts?_embed&slug=${slug}`),
        {
            select,
            onError
        }
    )
}

export default usePostsSlugShow;

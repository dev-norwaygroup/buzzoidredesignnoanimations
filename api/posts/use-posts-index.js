import { useQuery } from "react-query";
import http from '../http';

const usePostsIndex = (config) => {
    const select = (response) => response;
    const onError = (error) => console.log(error);

    let url = '/posts?_embed';

    if (config.postsPerPage) {
        url += `&per_page=${config.postsPerPage}`;
    }

    if (config.page) {
        url += `&page=${config.page}`;
    }

    return useQuery(
        ['posts', config.postsPerPage, config.page],
        () => http.get(url),
        {
            select,
            onError
        }
    )
}

export default usePostsIndex;

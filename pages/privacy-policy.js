
/**
 * Internal Dependencies
 */
import Head from 'next/head';
import { Trans } from '@lingui/macro';

/**
 * Internal dependencies
 */
import { loadTranslation } from '../utils/utils';

const PrivacyPolicy = () => {
    return (
        <>
            <Head>
            <title>Privacy Policy - Buzzoid</title>
                <meta
                    name="description"
                    content="Privacy Policy We take your privacy seriously and will take all measures to protect your personal information. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. Any personal information received will only be used to fill your order. We …"
                />
               <meta property="og:title" content="Privacy Policy - Buzzoid"
               />
                <meta property="og:description" content="Privacy Policy We take your privacy seriously and will take all measures to protect your personal information. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. Any personal information received will only be used to fill your order. We …"
                />
                <meta property="og:url" content="https://buzzoid.com/privacy-policy/" />
            </Head>
            <div className="section__wrapper">
                <div className="shell">
                    <div className="section__inner">
                        <div className="section__head">
                            <div>
                                <h1><Trans>Privacy Policy</Trans></h1>
                                <h4><Trans>Published on May 18, 2018</Trans></h4>
                            </div>
                        </div>
                        <div className="section__body">
                            <div>
                                <p><b><Trans>Privacy Policy</Trans></b></p>
                                <p><span><Trans>We take your privacy seriously and will take all measures to protect your personal information. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. Any personal information received will only be used to fill your order. We will not sell or redistribute your information to anyone.</Trans></span></p>
                                <p><span><Trans>We are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained.</Trans></span></p>
                                <p><b><Trans>The basics</Trans></b></p>
                                <ol>
                                    <li><span><Trans>Before or at the time of collecting personal information, we will identify the purposes for which information is being collected.</Trans></span></li>
                                    <li><span><Trans>We will collect and use of personal information solely with the objective of fulfilling those purposes specified by us and for other compatible purposes, unless we obtain the consent of the individual concerned or as required by law.</Trans></span></li>
                                    <li><span><Trans>We will only retain personal information as long as necessary for the fulfillment of those purposes.</Trans></span></li>
                                    <li><span><Trans>We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned.</Trans></span></li>
                                    <li><span><Trans>Personal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date.</Trans></span></li>
                                    <li><span><Trans>We will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.</Trans></span></li>
                                    <li><span><Trans>We will make readily available to customers information about our policies and practices relating to the management of personal information.</Trans></span></li>
                                    <li><span><Trans>Some payments are processed using PayPal&apos;s secure system while others are processed using a secure payment gateway, with no sensitive financial information being input through our web site. Secure method of accepting payments is processed using SSL encryption. </Trans></span></li>
                                    <li><span><Trans>We may collect and store limited payment information from you, such as payment card type and expiration date and the last four digits of your payment card number; however, we do not collect or store full payment card numbers and all transactions are processed by our third-party payment processor.</Trans></span></li>
                                </ol>
                                <p><b><Trans>Use of Information</Trans></b></p>
                                <p><span><Trans>We use information to provide, analyze, administer, enhance and personalize our services and marketing efforts, to process your registration, your orders and your payments, and to communicate with you on these and other topics. For example, we use information to: </Trans></span></p>
                                <ul>
                                    <li><span><Trans>prevent, detect and investigate potentially prohibited or illegal activities, including fraud, and enforcing our terms;</Trans></span></li>
                                    <li><span><Trans>analyze and understand our audience, improve our service (including our user interface experiences) </Trans></span></li>
                                </ul>
                                <p><b><Trans>Disclosure of Information </Trans></b></p>
                                <p><span><Trans>We use other companies, agents or contractors (“Service Providers”) to perform services on our behalf or to assist us with the provision of services to you. For example, we engage Service Providers to provide marketing, advertising, communications, infrastructure and IT services, to personalize and optimize our service, to process credit card transactions or other payment methods, to provide customer service, to collect debts, to analyze and enhance data (including data about users’ interactions with our service), and to process and administer consumer surveys. In the course of providing such services, these Service Providers may have access to your personal or other information. We do not authorize them to use or disclose your personal information except in connection with providing their services.</Trans></span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default PrivacyPolicy;

export const getStaticProps = async (ctx) => {
    const translation = await loadTranslation(
        ctx.locale,
        process.env.NODE_ENV === 'production'
    );

    return {
        props: {
            translation,
        },
    };
};

/**
 * Internal dependencies
 */
import { loadTranslation } from '../utils/utils';
import ContactForm from '../blocks/contact-form/contact-form';
import AnimateContact from '@/svg/animate-contact.svg';
import Animate from '@/components/animate/animate';
import Head from 'next/head';

const ContactUs = () => (
  <>
    <Head>
      <title>Contact Us - Buzzoid</title>
      <meta property="og:title" content="Contact Us - Buzzoid" />
      <meta property="og:url" content="https://buzzoid.com/contact-us/" />
    </Head>
    <Animate>
      <div className="shell">
        <div className="contact-us__inner">
          <div className="contact-us__svg">
            <AnimateContact />
          </div>
          <ContactForm />
        </div>
      </div>
    </Animate>
  </>
);

export default ContactUs;

export const getStaticProps = async (ctx) => {
  const translation = await loadTranslation(
    ctx.locale,
    process.env.NODE_ENV === 'production'
  );

  return {
    props: {
      translation,
    },
  };
};

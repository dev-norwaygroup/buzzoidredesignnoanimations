/**
 * Internal Dependencies
 */
import Head from 'next/head';
import { Trans } from '@lingui/macro';

/**
 * Internal dependencies
 */
import { loadTranslation } from '../utils/utils';

const TermsOfService = () => {
    return (
        <>
            <Head>
                <title>Terms of Service - Buzzoid</title>
                <meta
                    name="description"
                    content="Terms of use Thanks for using our products and services (“Services”). The Services are provided by Perago LLC, +1 855-848-9812. By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any …"
                />
               <meta property="og:title" content="Terms of Service - Buzzoid"
               />
                <meta property="og:description" content="Terms of use Thanks for using our products and services (“Services”). The Services are provided by Perago LLC, +1 855-848-9812. By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any …"
                />
                <meta property="og:url" content="https://buzzoid.com/terms-of-service/" />
            </Head>
            <div className="section__wrapper">
                <div className="shell">
                    <div className="section__inner">
                        <div className="section__head">
                            <div>
                                <h1><Trans>Terms of Service</Trans></h1>
                                <h4><Trans>Published on May 8, 2019</Trans></h4>
                            </div>
                        </div>
                        <div className="section__body">
                            <div>
                                <p><b><Trans>Terms of use</Trans></b></p>
                                <p><span><Trans>Thanks for using our products and services (“Services”). The Services are provided by </Trans><strong> <Trans>Perago LLC</Trans></strong>, +1 855-848-9812. <Trans>By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this web site are protected by applicable copyright and trademark law</Trans>.</span></p>
                                <p><b><Trans>Use License</Trans></b></p>
                                <p><span><Trans>Permission is granted to temporarily download one copy of the materials (information or software) on Buzzoid&apos;s web site for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license, you may not</Trans>:</span></p>
                                <ul>
                                    <li><span><Trans>modify or copy the materials</Trans>;</span></li>
                                    <li><span><Trans>use the materials for any commercial purpose, or for any public display (commercial or non-commercial</Trans>);</span></li>
                                    <li><span><Trans>attempt to decompile or reverse engineer any software contained on Buzzoid&apos;s web site</Trans>;</span></li>
                                    <li><span><Trans>remove any copyright or other proprietary notations from the materials; or</Trans></span></li>
                                    <li><span><Trans>transfer the materials to another person or “mirror” the materials on any other server</Trans>.</span></li>
                                </ul>
                                <p><span><Trans>This license shall automatically terminate if you violate any of these restrictions and may be terminated by Buzzoid at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format</Trans>.</span></p>
                                <p><b><Trans>Disclaimer</Trans></b></p>
                                <p><span><Trans>The materials on Buzzoid&apos;s web site are provided “as is”. Buzzoid makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, Buzzoid does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its Internet web site or otherwise relating to such materials or on any sites linked to this site</Trans>.</span></p>
                                <p><b><Trans>Limitations</Trans></b></p>
                                <p><span><Trans>In no event shall Buzzoid or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption,) arising out of the use or inability to use the materials on Buzzoid&apos;s Internet site, even if Buzzoid or a Buzzoid&apos;s authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you</Trans>.</span></p>
                                <p><b><Trans>Billing</Trans></b></p>
                                <p><span><Trans>Payments on Buzzoid are processed using either PayPal&apos;s secure system or a secure and verified payment gateway, with no sensitive financial information being input through our web site. Secure method of accepting payments is processed using SSL encryption. By purchasing our services you are explicitly agreeing that you clearly understand and agree to what you are purchasing and will not file a fraudulent dispute via PayPal, credit card or debit card institution. Furthermore, you have authorization to use the credit card, debit card, PayPal account or any other payment source used to make the purchase. Upon a fraudulent attempt to file a dispute or chargeback, we receive the right, if necessary, to reset all followers and likes, terminate your account and/or permanently ban your IP address. Users acknowledge chargebacks, disputes, or payment reversals will not be carried out prior to discussing the situation with our support team</Trans>.</span></p>
                                <p><b><Trans>Revisions and Errata</Trans></b></p>
                                <p><span><Trans>The materials appearing on Buzzoid&apos;s web site could include technical, typographical, or photographic errors. Buzzoid does not warrant that any of the materials on its web site are accurate, complete, or current. Buzzoid may make changes to the materials contained on its web site at any time without notice. Buzzoid does not, however, make any commitment to update the materials</Trans>.</span></p>
                                <p><b><Trans>Links</Trans></b></p>
                                <p><span><Trans>Buzzoid has not reviewed all of the sites linked to its Internet web site and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Buzzoid of the site. Use of any such linked web site is at the user&apos;s own risk</Trans>.</span></p>
                                <p><b><Trans>Site Terms of Use Modifications</Trans></b></p>
                                <p><span><Trans>Buzzoid may revise these terms of use for its web site at any time without notice. By using this web site you are agreeing to be bound by the then current version of these Terms and Conditions of Use</Trans>.</span></p>
                                <p><b><Trans>Governing Law</Trans></b></p>
                                <p><span><Trans>Any claim relating to Buzzoid&apos;s web site shall be governed by the laws of the United Arab Emirates without regard to its conflict of law provisions. General Terms and Conditions are applicable to Use of a Web Site</Trans>.</span></p>
                                <p><b><Trans>Support</Trans></b></p>
                                <p><span><Trans>We strive to provide our users with industry-leading support. For any sales or technical questions, please email us at support@buzzoid.com</Trans>.</span></p>
                                <p><b><Trans>Refunds</Trans></b></p>
                                <p><span><Trans>Customers who are not completely satisfied with our services may request a full refund within the first 30 days of their purchase. Refunds will be processed through our customer support agents who can be reached at support@buzzoid.com. Refunds are not applicable to free credits or services. Buzzoid reserves the right to meet customer satisfaction and all refunds must be processed in due diligence and due process</Trans>.</span></p>
                                <p><b><Trans>Service</Trans></b></p>
                                <ol>
                                    <li><span><Trans>Buzzoid is not affiliated with Instagram, Facebook or any Instagram third-party partners in any way. </Trans></span></li>
                                    <li><span><Trans>It is your sole responsibility to comply with Instagram rules and any legislation that you are subject to. You use Buzzoid at your own risk. </Trans></span></li>
                                    <li><span><Trans>We are not responsible for your actions and their consequences. We are not to blame if your Instagram account is banned for any reason. </Trans></span></li>
                                    <li><span><Trans>We require your Instagram username to obtain required information for the Instagram API. We do not store, give away, or otherwise distribute your username to any third parties</Trans>.</span></li>
                                    <li><span><Trans>The expected number of followers, likes, and views is not guaranteed to you in any way. </Trans></span></li>
                                    <li><span><Trans>We can&apos;t guarantee the continuous, uninterrupted or error-free operability of the services</Trans>.</span></li>
                                    <li><span><Trans>It is the sole responsibility of the customers to ensure their accounts are set to “public” during their use of the Buzzoid service. Any downtime of service in relation to a client changing their profile to “private” will not result in any payment reimbursement for that period of time. </Trans></span></li>
                                    <li><span><Trans>You agree that upon purchasing our service, that you clearly understand and agree on what you are purchasing and will not file a fraudulent dispute via the payment processor. </Trans></span></li>
                                    <li><span><Trans>We reserve the right to modify, suspend or withdraw the whole or any part of our service or any of its content at any time without notice and without incurring any liability. </Trans></span></li>
                                    <li><span><Trans>It is your sole responsibility to check whether the Terms have changed. </Trans></span></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default TermsOfService;

export const getStaticProps = async (ctx) => {
    const translation = await loadTranslation(
        ctx.locale,
        process.env.NODE_ENV === 'production'
    );

    return {
        props: {
            translation,
        },
    };
};

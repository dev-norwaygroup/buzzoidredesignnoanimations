import Head from 'next/head';
import { Trans } from '@lingui/macro';

/**
 * Internal Dependencies
 */
import { loadTranslation } from '../../utils/utils';
import Animate from '@/components/animate/animate';
import Offers from '@/components/offers/offers';
import CustomerRated from '@/components/customer-rated/customer-rated';
import FancyButtonGroup from '@/components/button/fancy-button-group';
import PostHead from '@/components/post-head/post-head';
import PostsGroup from '@/components/posts-group/posts-group';
import Spinner from '@/components/spinner/spinner';
import IconMrBuzz from '@/svg/icon-mr-buzz.svg';
import usePostsSlugShow from 'api/posts/use-posts-slug-show';
import usePostsIndex from 'api/posts/use-posts-index';
import http from '../../api/http';
import BlogNavigation from '@/components/blog-navigation/blog-navigation';
import FancyButton from '@/components/button/fancy-button';

const Post = ({ staticProps }) => {
  let { data: post } = usePostsSlugShow(staticProps.id);
  const response = usePostsIndex({ postsPerPage: 4 });
  const otherPosts = response.data?.data;

  if (!post || !otherPosts) {
    return <Spinner />;
  }

  const youMayLikePosts = otherPosts.reduce((acc, curr) => {
    if (curr.id !== post[0].id && acc.length < 3) {
      acc.push(curr);
    }
    return acc;
  }, []);

  return (
    <div className="post">
      <Head>
        <title>{post[0].title.rendered}</title>
      </Head>
      <BlogNavigation
        routerActive={false}
        currentCategory={post[0].categories[0]}
      />
      <div className="post__intro has-border">
        <div className="shell">
          <div className="post__intro-inner">
            <PostHead
              post={post[0]}
              isColumn={true}
              isReverse={true}
              isLink={false}
            />
            <div
              className="post__intro-content"
              dangerouslySetInnerHTML={{ __html: post[0].content.rendered }}
            ></div>
            <div className="mr-buzz">
              <div className="mr-buzz__image">
                <IconMrBuzz />
              </div>
              <span>Mr Buzz</span>
            </div>
          </div>
        </div>
      </div>
      <Animate>
        <PostsGroup title="You May Also Like" posts={youMayLikePosts} />
      </Animate>
      <Animate>
        <Offers>
          <FancyButtonGroup>
            <FancyButton
              href="/buy-instagram-followers"
              price="$2.97"
              isMostPopular={true}
              heading={<Trans>Buy Followers</Trans>}
            />
            <FancyButton
              href="/buy-instagram-likes"
              price="$1.47"
              heading={<Trans>Buy Likes</Trans>}
            />
            <FancyButton
              href="/buy-instagram-views"
              price="$1.99"
              heading={<Trans>Buy Views</Trans>}
            />
          </FancyButtonGroup>
          <CustomerRated />
        </Offers>
      </Animate>
    </div>
  );
};

export default Post;

export async function getStaticPaths({ locales }) {
  const posts = await http.get('/posts').then((res) => res.data);
  let paths = [];

  posts.map((post) => {
    for (const locale of locales) {
      paths.push({
        params: { id: post.slug },
        locale,
      });
    }
  });

  return {
    paths,
    fallback: false,
  };
}

export const getStaticProps = async (ctx) => {
  const id = ctx.params.id;
  const data = {
    id: id,
  };

  const translation = await loadTranslation(
    ctx.locale,
    process.env.NODE_ENV === 'production'
  );

  return {
    props: {
      translation,
      staticProps: data,
    },
  };
};

/**
 * External dependecies
 */
import Head from 'next/head';
import { useState } from 'react';

/**
 * Internal dependencies
 */
import { loadTranslation } from '../../utils/utils';
import usePostsIndex from 'api/posts/use-posts-index';
import Spinner from '@/components/spinner/spinner';
import BlogTemplate from '@/components/blog-template/blog-template';

const Blog = () => {
  const [currentPage, setCurrentPage] = useState(1);
  let postsPerPage = 9;

  if (typeof window !== 'undefined') {
    if (window.matchMedia('(max-width: 1023px)').matches) {
      postsPerPage = 4;
    }
  }

  const response = usePostsIndex({
    postsPerPage,
    page: currentPage,
  });

  const posts = response.data?.data;
  const avaiablePages = response.data?.headers['x-wp-totalpages'];

  if (!posts) {
    return <Spinner />;
  }

  return (
    <>
      <Head>
        <title>
          Instagram Marketing Blog: Strategies, Tips & Guides | Buzzoid
        </title>
        <meta
          name="description"
          content="Get the freshest and latest news about Instagram marketing directly from Buzzoid. We provide you with all the best guides to grow your Instagram account."
        />
      </Head>
      <BlogTemplate
        routerActive={false}
        avaiablePages={avaiablePages}
        posts={posts}
        setCurrentPage={setCurrentPage}
        currentPage={currentPage}
      />
    </>
  );
};

export default Blog;

export const getStaticProps = async (ctx) => {
  const translation = await loadTranslation(
    ctx.locale,
    process.env.NODE_ENV === 'production'
  );

  return {
    props: {
      translation,
    },
  };
};

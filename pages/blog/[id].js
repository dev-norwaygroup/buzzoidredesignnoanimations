/**
 * External dependencies
 */
import { useState, useEffect } from 'react';

/**
 * Internal dependencies
 */
import { loadTranslation } from '../../utils/utils';
import usePostsCategoryShow from 'api/posts/use-posts-category-show';
import Spinner from '@/components/spinner/spinner';
import http from '../../api/http';
import BlogTemplate from '@/components/blog-template/blog-template';

const Blog = ({ staticProps }) => {
  const [currentPage, setCurrentPage] = useState(1);
  let postsPerPage = 9;

  if (typeof window !== 'undefined') {
    if (window.matchMedia('(max-width: 1023px)').matches) {
      postsPerPage = 4;
    }
  }

  const response = usePostsCategoryShow(staticProps.id, {
    postsPerPage,
    page: currentPage,
  });
  const posts = response.data?.data;
  const avaiablePages = response.data?.headers['x-wp-totalpages'];

  if (!posts) {
    return <Spinner />;
  }

  return (
    <BlogTemplate
      avaiablePages={avaiablePages}
      posts={posts}
      setCurrentPage={setCurrentPage}
      currentPage={currentPage}
      checkCategory={true}
    />
  );
};

export default Blog;

export async function getStaticPaths() {
  const posts = await http.get('/posts').then((res) => res.data);
  return {
    paths: posts.map((post) => {
      return {
        params: { id: post.categories[0].toString() },
      };
    }),
    fallback: false,
  };
}

export const getStaticProps = async (ctx) => {
  const id = ctx.params.id;
  const data = {
    id: id,
  };

  const translation = await loadTranslation(
    ctx.locale,
    process.env.NODE_ENV === 'production'
  );

  return {
    props: {
      translation,
      staticProps: data,
    },
  };
};

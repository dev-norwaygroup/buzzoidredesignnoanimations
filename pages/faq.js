/**
 * External dependencies
 */
import Head from 'next/head';
import { Trans } from '@lingui/macro';

/**
 * Internal dependencies
 */
import { loadTranslation } from '../utils/utils';
import FaqIntro from '@/components/faq-intro/faq-intro';
import Offers from '@/components/offers/offers';
import FancyButtonGroup from '@/components/button/fancy-button-group';
import CustomerRated from '@/components/customer-rated/customer-rated';
import Animate from '@/components/animate/animate';
import FancyButton from '@/components/button/fancy-button';

const Faq = () => {
  return (
    <>
      <Head>
        <title>Frequently Asked Questions</title>
        <meta
          name="description"
          content="Questions that you may want to ask that have been asked before. Read through them to make sure you're on the right page."
        />
        <meta
          property="og:title"
          content="Frequently Asked Questions - Buzzoid"
        />
        <meta
          property="og:description"
          content="Questions that you may want to ask that have been asked before. Read through them to make sure you&#039;re on the right page."
        />
        <meta property="og:url" content="https://buzzoid.com/faq/" />
      </Head>
      <Animate>
        <FaqIntro />
      </Animate>
      <Animate>
        <Offers>
          <FancyButtonGroup>
            <FancyButton
              href="/buy-instagram-followers"
              price="$2.97"
              isMostPopular={true}
              heading={<Trans>Buy Followers</Trans>}
            />
            <FancyButton
              href="/buy-instagram-likes"
              price="$1.47"
              heading={<Trans>Buy Likes</Trans>}
            />
            <FancyButton
              href="/buy-instagram-views"
              price="$1.99"
              heading={<Trans>Buy Views</Trans>}
            />
          </FancyButtonGroup>
          <CustomerRated />
        </Offers>
      </Animate>
    </>
  );
};

export default Faq;

export const getStaticProps = async (ctx) => {
  const translation = await loadTranslation(
    ctx.locale,
    process.env.NODE_ENV === 'production'
  );

  return {
    props: {
      translation,
    },
  };
};

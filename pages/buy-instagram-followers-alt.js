import Head from 'next/head';
import Image from 'next/image';
import { Trans } from '@lingui/macro';
import * as yup from 'yup';

/**
 * Internal dependencies
 */
import { loadTranslation } from '../utils/utils';
import Animate from '@/components/animate/animate';
import Logos from '@/components/logos/logos';
import Heading from '@/components/heading/heading';
import Reviews from '@/components/reviews/reviews';
import { ReviewsFollowersSchema } from 'statics/reviews';
import { buyFollowersItems } from 'statics/faq';
import BuyForm from '@/blocks/buy-form/buy-form';
import Offers from '@/components/offers/offers';
import { followersIcons } from 'statics/icons';
import FollowersData from '@/blocks/buy-form/data/data-followers';
import { followersReviews } from 'statics/reviews-temp';

import Grid from '@/components/grid/grid';
import BoxAnimate from '@/components/box-animate/box-animate';
import Faqs from '@/components/faqs/faqs';
import BuyFormAlt from '@/blocks/buy-form-alt/buy-form-alt';
import FollowersAltData from '@/blocks/buy-form-alt/data/data-followers-alt';
import CustomerStories from '@/components/customer-stories/customer-stories';

import AnimateDelivery from '@/svg/animate-delivery.svg';
import AnimateGurantee from '@/svg/animate-guarantee.svg';
import AnimateSupport from '@/svg/animate-support.svg';
import CustomerRated from '@/components/customer-rated/customer-rated';

import IntroImage from '../assets/images/intro-static5.png';

const schema = yup.object().shape({
  type: yup.string().required(),
  followers: yup
    .string()
    .nullable()
    .required('Please choose a number of follwers'),
});

const BuyFollowersAlt = () => (
  <>
    <Head>
      <title>Buy Instagram Followers - 100% Real & Instant | Now $2.97</title>
      <meta
        name="description"
        content="Buy Instagram followers from Buzzoid for as little as $2.97. Instant
        delivery, real followers, and friendly 24/7 customer support. Try us
        today."
      />
      <meta
        property="og:title"
        content="Buy Instagram Followers - 100% Real &amp; Instant | Now $2.97"
      />
      <meta
        property="og:description"
        content="Buy Instagram followers from Buzzoid for as little as $2.97. Instant delivery, real followers, and friendly 24/7 customer support. Try us today."
      />
      <meta
        property="og:url"
        content="https://buzzoid.com/buy-instagram-followers/"
      />
    </Head>
    <Animate>
      <div className="intro">
        <div className="shell">
          <div className="intro__inner">
            <div className="intro__entry smaller">
              <h1>
                <Trans>Buy Instagram Followers with Instant Delivery</Trans>
              </h1>
              <p className="fullwidth">
                <Trans>
                  At Buzzoid, you can buy Instagram followers quickly, safely
                  and easily with just a few clicks. See our deals below!
                </Trans>
              </p>
              <CustomerRated className="mobile-only" />
              <BuyFormAlt fields={FollowersAltData} schema={schema} />
            </div>
            <div className="intro__img">
              <Image
                src={IntroImage}
                width={557}
                height={580}
                alt="intro image"
              />
            </div>
          </div>
        </div>
      </div>
    </Animate>
    <Animate>
      <Logos />
    </Animate>
    <Animate>
      <Heading className="ta-c">
        <h2>
          <Trans>Ready to Buy Instagram Followers?</Trans>
        </h2>
        <p>
          <Trans>
            Buying likes for your Instagram posts is the best way to gain more
            engagement and success. Improve your social media marketing strategy
            with Buzzoid.
          </Trans>
        </p>
      </Heading>
    </Animate>
    <Animate>
      <Grid grid="3">
        <BoxAnimate>
          <BoxAnimate.Image>
            <AnimateDelivery className="animate-delivery" />
          </BoxAnimate.Image>
          <BoxAnimate.Content>
            <h4>
              <Trans>Fastest delivery</Trans>
            </h4>
            <p>
              <Trans>
                We provide you with the fastest Instagram Followers and Likes in
                the market. At Buzzoid, you will receive all of your Likes and
                Followers within an hour after completing your order.
              </Trans>
            </p>
          </BoxAnimate.Content>
        </BoxAnimate>
        <BoxAnimate>
          <BoxAnimate.Image>
            <AnimateGurantee className="animate-gurantee" />
          </BoxAnimate.Image>
          <BoxAnimate.Content>
            <h4>
              <Trans>Our guarantee</Trans>
            </h4>
            <p>
              <Trans>
                We want to leave a lasting impression on our clients. If you
                aren&apos;t satisfied with the quality or delivery of your
                order, tell us. We&apos;ll refund any order that isn&apos;t
                fulfilled.
              </Trans>
            </p>
          </BoxAnimate.Content>
        </BoxAnimate>
        <BoxAnimate>
          <BoxAnimate.Image>
            <AnimateSupport className="animate-support" />
          </BoxAnimate.Image>
          <BoxAnimate.Content>
            <h4>
              <Trans>24-hour support</Trans>
            </h4>
            <p>
              <Trans>
                Our dedicated support staff is always available. If you have any
                questions about our services or experience any problems with
                your order, please don&apos;t hesitate to contact us.
              </Trans>
            </p>
          </BoxAnimate.Content>
        </BoxAnimate>
      </Grid>
    </Animate>
    <Animate>
      <Heading className="ta-c">
        <h2>
          <Trans>Buy Instagram Followers Easily With Buzzoid</Trans>
        </h2>
        <p className="fullwidth">
          Most individuals are totally unwilling to invest time into a profile
          that has little interaction.
        </p>
      </Heading>
    </Animate>
    <Animate>
      <div className="shell">
        <Faqs items={buyFollowersItems} />
      </div>
    </Animate>
    <div className="divider divider--mb-90"></div>
    <CustomerStories />
    <Animate>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify(ReviewsFollowersSchema),
        }}
      ></script>
      <Reviews data={followersReviews} />
    </Animate>
    <Animate>
      <Offers icons={followersIcons}>
        <BuyForm fields={FollowersData} schema={schema} prefix="offers" />
      </Offers>
    </Animate>
  </>
);

export default BuyFollowersAlt;

export const getStaticProps = async (ctx) => {
  const translation = await loadTranslation(
    ctx.locale,
    process.env.NODE_ENV === 'production'
  );

  return {
    props: {
      translation,
    },
  };
};

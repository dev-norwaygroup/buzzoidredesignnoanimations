import Head from 'next/head';
import { Trans } from '@lingui/macro';
import * as yup from 'yup';

/**
 * Internal dependencies
 */
import { loadTranslation } from '../utils/utils';
import Animate from '@/components/animate/animate';
import Logos from '@/components/logos/logos';
import Heading from '@/components/heading/heading';
import { buyViewsItems } from 'statics/faq';
import { ReviewsFollowersSchema } from 'statics/reviews';
import BuyForm from '@/blocks/buy-form/buy-form';
import { viewsIcons } from '../statics/icons';

import LogoEntrepremeur from '@/svg/logo-entrepreneur.svg';
import LogoHuffpost from '@/svg/logo-huffpost.svg';
import LogoSej from '@/svg/logo-sej.svg';
import LogoVice from '@/svg/logo-vice.svg';
import LogoVox from '@/svg/logo-vox.svg';
import Grid from '@/components/grid/grid';
import BoxAnimate from '@/components/box-animate/box-animate';
import Faqs from '@/components/faqs/faqs';
import FlexRow from '@/components/flex-row/flex-row';
import ViewsData from '@/blocks/buy-form/data/data-views';
import CustomSVG from '@/components/custom-svg/custom-svg';
import CustomerStories from '@/components/customer-stories/customer-stories';
import Reviews from '@/components/reviews/reviews';

import AnimateDelivery from '@/svg/animate-delivery.svg';
import AnimateSupport from '@/svg/animate-support.svg';
import AnimateReal from '@/svg/animate-real.svg';

import IconLock from '@/svg/icon-lock.svg';
import IconLighting from '@/svg/icon-lighting.svg';
import IconPlay from '@/svg/icon-play.svg';
import IconUsers from '@/svg/icon-users.svg';
import IconSupport from '@/svg/icon-support.svg';
import Offers from '@/components/offers/offers';
import { viewsReviews } from 'statics/reviews-temp';

const schema = yup.object().shape({
  views: yup.string().nullable().required('Please choose a number of views'),
});

const BuyViews = () => (
  <>
    <Head>
      <title>Buy Instagram Views - Real & Instant Delivery | Now $1.99</title>
      <meta
        name="description"
        content="If you want to buy Instagram views, things are about to get a lot easier. Buzzoid has competitive prices, fast delivery speed, and friendly support."
      />
      <meta
        property="og:title"
        content="Buy Instagram Views - Real &amp; Instant Delivery | Now $1.99"
      />
      <meta
        property="og:description"
        content="If you want to buy Instagram views, things are about to get a lot easier. Buzzoid has competitive prices, fast delivery speed, and friendly support."
      />
      <meta
        property="og:url"
        content="https://buzzoid.com/buy-instagram-views/"
      />
    </Head>
    <Animate>
      <div className="intro">
        <div className="shell">
          <div className="intro__inner">
            <div className="intro__entry smaller">
              <h1>
                <Trans>Buy Instagram Views with Instant Delivery</Trans>
              </h1>
              <p className="fullwidth">
                <Trans>
                  At Buzzoid, you can buy Instagram followers quickly, safely
                  and easily with just a few clicks. See our deals below!
                </Trans>
              </p>
              <BuyForm fields={ViewsData} schema={schema} />
              <FlexRow grid="2" className="form-icons-grid">
                <FlexRow.Col>
                  <div className="form-icons-grid__icon">
                    <IconPlay width="15" height="17" />
                  </div>
                  Order start in 60 seconds
                </FlexRow.Col>
                <FlexRow.Col>
                  <div className="form-icons-grid__icon">
                    <IconLighting />
                  </div>
                  Fast delivery (gradual or instant)
                </FlexRow.Col>
                <FlexRow.Col>
                  <div className="form-icons-grid__icon">
                    <IconUsers />
                  </div>
                  High quality views
                </FlexRow.Col>
                <FlexRow.Col>
                  <div className="form-icons-grid__icon">
                    <IconSupport />
                  </div>
                  24/7 Live Support
                </FlexRow.Col>
                <FlexRow.Col>
                  <div className="form-icons-grid__icon">
                    <IconLock />
                  </div>
                  No password required
                </FlexRow.Col>
              </FlexRow>
            </div>
            <div className="intro__img">
              <CustomSVG type="views" />
            </div>
          </div>
        </div>
      </div>
    </Animate>
    <Animate>
      <Logos>
        <LogoEntrepremeur />
        <LogoHuffpost />
        <LogoSej />
        <LogoVice />
        <LogoVox />
      </Logos>
    </Animate>
    <Animate>
      <Heading className="ta-c">
        <h2>
          <Trans>Ready to Buy Instagram Views?</Trans>
        </h2>
        <p>
          <Trans>
            Instagram video views are an essential part of your marketing
            strategy. Buy Instagram views from Buzzoid, and watch your video
            gain more popularity and brand awareness.
          </Trans>
        </p>
      </Heading>
    </Animate>
    <Animate>
      <Grid grid="3">
        <BoxAnimate>
          <BoxAnimate.Image>
            <AnimateDelivery className="animate-delivery" />
          </BoxAnimate.Image>
          <BoxAnimate.Content>
            <h4>
              <Trans>Fastest delivery</Trans>
            </h4>
            <p>
              <Trans>
                Don&apos;t wait to get your views. We send instant views from
                real people.
              </Trans>
            </p>
          </BoxAnimate.Content>
        </BoxAnimate>
        <BoxAnimate>
          <BoxAnimate.Image>
            <AnimateReal className="animate-real" />
          </BoxAnimate.Image>
          <BoxAnimate.Content>
            <h4>
              <Trans>Our guarantee</Trans>
            </h4>
            <p>
              <Trans>
                Buying Instagram views doesn&apos;t have to be hard! We only
                deliver 100% organic views.
              </Trans>
            </p>
          </BoxAnimate.Content>
        </BoxAnimate>
        <BoxAnimate>
          <BoxAnimate.Image>
            <AnimateSupport className="animate-support" />
          </BoxAnimate.Image>
          <BoxAnimate.Content>
            <h4>
              <Trans>24-hour support</Trans>
            </h4>
            <p>
              <Trans>
                Nothing worse than dealing with bad customer support. We got
                your back.
              </Trans>
            </p>
          </BoxAnimate.Content>
        </BoxAnimate>
      </Grid>
    </Animate>
    <Animate>
      <Heading className="ta-c">
        <h2>
          <Trans>Buy Instagram Followers Easily With Buzzoid</Trans>
        </h2>
        <p className="fullwidth">
          Most individuals are totally unwilling to invest time into a profile
          that has little interaction.
        </p>
      </Heading>
    </Animate>
    <Animate>
      <div className="shell">
        <Faqs items={buyViewsItems} />
      </div>
    </Animate>
    <div className="divider divider--mb-90"></div>
    <CustomerStories />
    <Animate>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify(ReviewsFollowersSchema),
        }}
      ></script>
      <Reviews data={viewsReviews} />
    </Animate>
    <Animate>
      <Offers icons={viewsIcons}>
        <BuyForm fields={ViewsData} schema={schema} prefix="offers"/>
      </Offers>
    </Animate>
  </>
);

export default BuyViews;

export const getStaticProps = async (ctx) => {
  const translation = await loadTranslation(
    ctx.locale,
    process.env.NODE_ENV === 'production'
  );

  return {
    props: {
      translation,
    },
  };
};

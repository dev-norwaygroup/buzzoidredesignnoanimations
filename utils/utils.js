/**
 * External Dependencies
 */
import { Trans } from '@lingui/macro';
import { en, fr } from 'make-plural/plurals';

export function initTranslation(i18n) {
  i18n.loadLocaleData({
    en: { plurals: en },
    fr: { plurals: fr },
    pseudo: { plurals: en },
  });
}

export async function loadTranslation(locale, isProduction = true) {
  let data;
  if (isProduction) {
    data = await import(`../translations/locales/${locale}/messages`);
  } else {
    data = await import(
      `@lingui/loader!../translations/locales/${locale}/messages.po`
    );
  }

  return data.messages;
}

export const getCategoryDetails = (category, index) => {
  const isMultipleOfThree = index % 3 === 0;
  const isMultipleOfTwo = index % 2 === 0;
  let color, text;

  if (isMultipleOfThree) {
    color = 'orange';
  } else if (isMultipleOfTwo) {
    color = 'yellow';
  } else {
    color = 'blue';
  }

  if (category == 4) {
    text = <Trans>Instagram Followers</Trans>;
  } else if (category == 124) {
    text = <Trans>Instagram Analytics</Trans>;
  } else {
    text = <Trans>Instagram Marketing</Trans>;
  }

  return {
    color,
    text,
  };
};

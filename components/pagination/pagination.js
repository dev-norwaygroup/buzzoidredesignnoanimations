/**
 * External Dependencies
 */
import classNames from 'classnames';

/**
 * Internal Dependencies
 */
import IconChevronLeft from '@/svg/icon-chevron-left-black.svg';
import IconChevronRight from '@/svg/icon-chevron-right-black.svg';

const Pagination = ({
    pages,
    onPageClick,
    currentPage,
    onArrowBackClick,
    onArrowForwardClick
}) => {
    const arrayOfZeros = Array(Number(pages)).fill(0);

    return (
        <div className="pagination">
            <div className="shell">
                <div className="pagination__inner">
                    <div
                        className={classNames('pagination__arrow pagination__arrow--back', {'disabled': currentPage == 1})}
                        onClick={onArrowBackClick}
                    >
                        <IconChevronLeft />
                    </div>
                    <ul>
                        {
                            arrayOfZeros.map((x, index) => <li
                                className={classNames({'active': index + 1 == currentPage})}
                                key={index}
                                onClick={() => onPageClick(index + 1)}
                            >
                                {index + 1}
                            </li>
                            )
                        }
                    </ul>
                    <div
                        className={classNames('pagination__arrow pagination__arrow--forward', {'disabled': currentPage >= pages})}
                        onClick={onArrowForwardClick}
                    >
                        <IconChevronRight />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Pagination

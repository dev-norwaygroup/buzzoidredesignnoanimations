/**
 * External Dependencies
 */
import { Trans } from '@lingui/macro';

const navItems = [
  {
    content: <Trans>Buy Instagram Likes</Trans>,
    href: '/buy-instagram-likes',
  },
  {
    content: <Trans>Buy Instagram Followers</Trans>,
    href: '/buy-instagram-followers',
  },
  {
    content: <Trans>Buy Instagram Views</Trans>,
    href: '/buy-instagram-views',
  },
  {
    content: <Trans>FAQ</Trans>,
    href: '/faq',
  },
  {
    content: <Trans>Contact Us</Trans>,
    href: '/contact-us',
  },
];

export default navItems;

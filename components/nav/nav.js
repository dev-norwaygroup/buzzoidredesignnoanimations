/**
 * External Dependencies
 */
import Link from 'next/link';
import classNames from 'classnames';
import { useRouter } from 'next/router';

const Nav = ({ items, heading, className, currentCategory }) => {
  const router = useRouter();
  const locale = router.locale;

  return (
    <nav className={classNames('nav', className)}>
      {heading && <h3>{heading}</h3>}
      <ul>
        {items?.map((item) => (
          <li
            key={item.href}
            className={classNames({
              active:
                router.asPath === item.href ||
                router.asPath === `/${locale}/${item.href}` ||
                currentCategory == item.href.split('/').pop()
            })}
          >
            <Link href={item.href}>
              <a>{item.content}</a>
            </Link>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Nav;

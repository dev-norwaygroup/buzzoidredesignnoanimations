/**
 * External dependencies
 */
import { Trans } from '@lingui/macro';

/**
 * Internal dependencies
 */
import LogoEntrepremeur from '@/svg/logo-entrepreneur.svg';
import LogoHuffpost from '@/svg/logo-huffpost.svg';
import LogoSej from '@/svg/logo-sej.svg';
import LogoVice from '@/svg/logo-vice.svg';
import LogoVox from '@/svg/logo-vox.svg';

const Logos = () => (
  <div className="logos has-border">
    <div className="shell">
      <h4>
        <Trans>As seen on</Trans>
      </h4>

      <div className="logos__inner">
        <div className="logos__col">
          <LogoEntrepremeur className="entp" />
          <LogoHuffpost className="huff" />

        </div>
        <div className="logos__col">
          <LogoSej className="sej" />
          <LogoVice className="vice" />
          <LogoVox className="vox" />
        </div>
      </div>
    </div>
  </div>
);

export default Logos;

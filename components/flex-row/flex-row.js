import classNames from 'classnames';
import React from 'react';

const FlexCol = ({ children, className, style }) => (
  <div className={classNames('flex-col', className)} style={style}>
    {children}
  </div>
);
const FlexRow = ({ children, className, grid, style }) => (
  <div
    className={classNames('flex-row', className, {
      [`flex-row--grid-${grid}`]: grid,
    })}
    style={style}
  >
    {children}
  </div>
);

FlexRow.Col = FlexCol;

export default FlexRow;

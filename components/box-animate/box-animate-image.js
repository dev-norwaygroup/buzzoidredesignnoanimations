const BoxAnimateImage = ({ children }) => (
  <div className="box-animate__image">{children}</div>
);

export default BoxAnimateImage;

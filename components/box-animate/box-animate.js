import BoxAnimateImage from '@/components/box-animate/box-animate-image';
import BoxAnimateContent from '@/components/box-animate/box-animate-content';

const BoxAnimate = ({ children }) => (
  <div className="box-animate">{children}</div>
);

BoxAnimate.Image = BoxAnimateImage;
BoxAnimate.Content = BoxAnimateContent;

export default BoxAnimate;

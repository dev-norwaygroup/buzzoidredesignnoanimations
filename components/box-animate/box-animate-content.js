const BoxAnimateContent = ({ children }) => (
  <div className="box-animate__content">{children}</div>
);

export default BoxAnimateContent;

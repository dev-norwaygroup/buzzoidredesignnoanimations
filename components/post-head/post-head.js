/**
 * External Dependencies
 */
import Image from "next/image";
import personImg from '../../assets/images/person.png';
import classNames from "classnames";
import Link from "next/link";
import { Trans } from "@lingui/macro";

/**
 * Internal Dependencies
 */
import { getCategoryDetails } from '../../utils/utils';

const PostHead = ({
    index = 0,
    isLink = true,
    post,
    isSmall,
    isColumn,
    isReverse,
    className
}) => {
    const color = getCategoryDetails(post.categories[0], index + 1).color;
    const text = getCategoryDetails(post.categories[0], index + 1).text;
    const fullDate = new Date(post.date);
    const year = fullDate.getFullYear();
    const date = fullDate.getDate();
    const month = fullDate.toLocaleString('default', { month: 'long' });
    const thumbnailImg = post._embedded['wp:featuredmedia'] && post._embedded['wp:featuredmedia'][0].source_url;
    const imgSrc = thumbnailImg ? thumbnailImg : personImg;

    const InnerTemplate = () => (
        <div className="post-head__inner">
            <div className="post-head__img-block">
                <div className="post-head__img-wrapper">
                    <Image
                        src={imgSrc}
                        alt="Media picture"
                        objectFit="cover"
                        layout="fill"
                        unoptimized
                        priority
                    />
                </div>
            </div>
            <div className="post-head__entry">
                <span className="post-head__category">{text}</span>
                <h3>{post.title.rendered}</h3>
                <span className="post-head__date"><Trans>Updated </Trans> {month} {date}, {year}</span>
            </div>
        </div>
    );

    return (
        <div className={ classNames(
                'post-head',
                className,
                color,
                {
                    'is-small': isSmall,
                    'is-column': isColumn,
                    'is-reverse': isReverse,
                    'no-img': !thumbnailImg
                }
            )
        }
        >
            {
                isLink
                    ? <Link href={`/posts/${post.slug}`} passHref>
                        <a>
                            <InnerTemplate />
                        </a>
                      </Link>
                    : <InnerTemplate />
            }
        </div>
    )
}

export default PostHead;
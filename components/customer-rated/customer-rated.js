/**
 * External Dependencies
 */
import classNames from 'classnames';
import { Trans } from '@lingui/macro';

/**
 * Internal Dependencies
 */
import IconStars from '@/svg/icon-stars.svg';

const CustomerRated = ({ isColumn, className }) => (
  <div
    className={classNames('customer-rated', className, {
      'is-column': isColumn,
    })}
  >
    <div className="customer-rated__inner">
      <div className="customer-rated__stars">
        <IconStars />
      </div>
      <div className="customer-rated__text">
        <span className="customer-rated__rate">
          <Trans>Rated</Trans>
          <span className="customer-rated__number">5.0</span>
        </span>
        <Trans>for Customer Service</Trans>
      </div>
    </div>
  </div>
);

export default CustomerRated;

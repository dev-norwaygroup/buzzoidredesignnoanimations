/**
 * External Dependencies
 */
import classNames from "classnames";

/**
 * Internal Dependencies
 */
import PostHead from "../post-head/post-head";

const PostsGroup = ({
    seekForFirst = false,
    title,
    posts
}) => {
    return (
        <div className="posts-group">
            <div className="shell">
                <div className="posts-group__inner">
                    <div className="posts-group__heading">
                        <h3>{title}</h3>
                    </div>
                    <div className="posts-group__collection">
                        {
                            posts.map((post, index) => <PostHead
                                className={classNames({'is-first': seekForFirst && index === 0 })}
                                post={post}
                                index={index}
                                isColumn={true}
                                isSmall={true}
                                key={post.id}
                            />
                            )
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PostsGroup

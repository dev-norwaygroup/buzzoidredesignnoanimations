/**
 * External dependencies
 */
import classNames from 'classnames';

/**
 * Internal Dependencies
 */
import BoxRowCol from './box-row-col.js';

const BoxRow = ({ children, className, style, refProp }) => (
  <div className="box-row" style={style} ref={refProp}>
    <div className="shell">
      <div className={classNames('box-row__inner', className)}>{children}</div>
    </div>
  </div>
);

BoxRow.Col = BoxRowCol;

export default BoxRow;

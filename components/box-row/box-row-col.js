const BoxRowCol = ({ children, size, style }) => (
  <div className="box-row__col" data-size={size} style={style}>
    {children}
  </div>
);

export default BoxRowCol;

/**
 * External Dependencies
 */
import { Trans } from '@lingui/macro';
/**
 * Internal Dependencies
 */
import Faqs from '@/components/faqs/faqs';
import Button from '@/components/button/button';
import AnimateFaq from '@/svg/animate-faq.svg';
import IconRocket from '@/svg/icon-rocket.svg';

const FaqIntro = () => (
  <div className="faq-intro">
    <div className="shell">
      <div className="faq-intro__inner">
        <div className="faq-intro__svg">
          <AnimateFaq />
        </div>
        <div className="faq-intro__faqs-wrapper">
          <Faqs>
            <h2 className="h1-like">
              <Trans>Frequently Asked Questions</Trans>
            </h2>
          </Faqs>
          <div className="faq-intro__contact">
            <p>
              <Trans>To receive more information about Buzzoid service:</Trans>
            </p>
            <Button href="/contact-us" className="btn--white">
              <Trans>Contact Us</Trans>
              <span>
                <IconRocket />
              </span>
            </Button>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default FaqIntro;

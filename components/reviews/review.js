/**
 * Internal Dependencies  
 */
import IconCheck from '@/svg/icon-check.svg';
import IconUpQuotes from '@/svg/icon-up-quotes.svg';
import IconDownQuotes from '@/svg/icon-down-quotes.svg';
import AuthorBox from '../author-box/author-box';
import Avatar from '../../assets/images/no-avatar.png';
import IconStarFill from '../../assets/images/svg/icon-star-fill.svg';

const Review = ({
    text,
    stars,
    author
}) => {
    const arrayOfStars = Array(stars).fill(0);
    
    return (
        <div className="review">
            <div className="review__header">
                <div className="review__stars">
                    {
                        arrayOfStars.map((x, index) => <IconStarFill key={index}/>)
                    }
                </div>
                <p className="review__text">{text}</p>
                <div className="review__quote review__quote--down">
                    <IconDownQuotes />
                </div>
                <div className="review__quote review__quote--up">
                    <IconUpQuotes />
                </div>
            </div>
            <div className="review__footer">
                <div className="review__picture">
                    <AuthorBox avatar={Avatar} />
                </div>
                <div className="review__author">
                    <h5>@{author} <IconCheck /></h5>
                    <p>on his instagram Likes Purchase</p>
                </div>
            </div>
        </div>
    )
}

export default Review;

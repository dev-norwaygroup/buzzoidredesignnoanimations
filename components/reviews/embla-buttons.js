/**
 * Internal Dependencies
 */
import IconChevronRight from '@/svg/icon-chevron-right-slider.svg';
import IconChevronLeft from '@/svg/icon-chevron-left-gray.svg';

export const DotButton = ({ selected, onClick }) => (
    <div
        className={`embla__dot ${selected ? "is-selected" : ""}`}
        onClick={onClick}
    >
    </div>
);

export const PrevButton = ({ onClick }) => (
    <div className="embla__button embla__button--prev" onClick={onClick}>
        <IconChevronLeft />
    </div>
);

export const NextButton = ({ onClick }) => (
    <div className="embla__button embla__button--next" onClick={onClick}>
        <IconChevronRight />
    </div>
);
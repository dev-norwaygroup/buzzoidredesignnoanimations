const Animate = ({ children }) => (
  <div className="animate aos-animate">{children}</div>
);

export default Animate;

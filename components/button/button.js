/**
 * External Dependencies
 */
import Link from "next/link"
import classNames from "classnames"

const Button = ({ children, href, className, onClick }) => (
  <Link href={href}>
    <a onClick={onClick} className={classNames('btn', className)}>
      {children}
    </a>
  </Link>
)

export default Button

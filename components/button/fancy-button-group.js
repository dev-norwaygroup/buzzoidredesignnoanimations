/**
 * External Dependencies
 */
import classNames from 'classnames';

const FancyButtonGroup = ({ isRow, children }) => (
  <div className={classNames('fancy-btn-group', { 'is-row': isRow })}>
    {children}
  </div>
);

export default FancyButtonGroup;

/**
 * External Dependencies
 */
import classNames from 'classnames';
import Link from 'next/link';
import { Trans } from '@lingui/macro';

/**
 * Internal Dependencies
 */
import IconChevronRight from '@/svg/icon-chevron-right.svg';

const FancyButton = ({ href, heading, isMostPopular, price }) => {
  return (
    <Link href={href}>
      <a className={classNames('fancy-btn', { 'is-popular': isMostPopular })}>
        <div className="fancy-btn__entry">
          <h5>{heading}</h5>
          <h6>
            <Trans>Starting at</Trans>
            <span>{price}</span>
          </h6>
        </div>
        <div className="fancy-btn__arrow">
          <IconChevronRight />
        </div>
      </a>
    </Link>
  );
};

export default FancyButton;

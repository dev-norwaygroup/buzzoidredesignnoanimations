import classNames from 'classnames';

const Grid = ({ children, grid, className, fullwidth }) => (
  <div className="grid">
    <div className={classNames('shell', { [`shell--fullwidth`]: fullwidth })}>
      <div
        className={classNames('grid__inner', `grid__inner--${grid}`, className)}
      >
        {children}
      </div>
    </div>
  </div>
);

export default Grid;

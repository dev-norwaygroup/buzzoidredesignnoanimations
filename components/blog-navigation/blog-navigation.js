/**
 * External Dependencies
 */
import { useRouter } from 'next/router';

/**
 * Internal Dependencies
 */
import Dropdown from '@/components/dropdown/dropdown';
import Nav from '@/components/nav/nav';

const BlogNavigation = ({ items, checkCategory = false, currentCategory }) => {
  const router = useRouter();

  if (checkCategory) {
    currentCategory = checkCategory && router.asPath.split('/').pop();
  }

  const navItems = [
    {
      content: 'Blog',
      href: '/blog',
    },
    {
      content: 'Instagram Analytics',
      href: '/blog/124',
    },
    {
      content: 'Instagram Followers',
      href: '/blog/4',
    },
    {
      content: 'Instagram Marketing',
      href: '/blog/161',
    },
  ];

  return (
    <div className="blog-navigation">
      <div className="shell">
        <Nav
          items={items ? items : navItems}
          className="nav-blog"
          currentCategory={currentCategory}
        />
        <Dropdown isNavigation={true} currentCategory={currentCategory} />
      </div>
    </div>
  );
};

export default BlogNavigation;

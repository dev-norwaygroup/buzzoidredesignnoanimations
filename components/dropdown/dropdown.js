/**
 * Internal Dependencies
 */
import { DropDownProvider } from './use-dropdown-context';
import DropdownMain from './dropdown-main';
import DropdownNavigation from './dropdown-navigation';

const Dropdown = (props) => (
  <DropDownProvider>
    {props.isNavigation ? (
      <DropdownNavigation {...props} />
    ) : (
      <DropdownMain {...props} />
    )}
  </DropDownProvider>
);

export default Dropdown;

/**
 * External Dependencies
 */
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import classNames from 'classnames';
/**
 * Internal Dependencies
 */
import DropdownButton from './dropdown-button';
import DropdownItem from './dropdown-item';
import { useDropdown } from './use-dropdown-context';

const DropdownMain = ({ className, defaultComponent, items }) => {
  const router = useRouter();
  const { setItem, setIsShow, isShow, clicked } = useDropdown();
  const [locale, setLocale] = useState(router.locale);
  const handleListClick = () => {
    if (isShow) {
      setIsShow(false);
    }
  };

  useEffect(() => {
    setItem(items.find((item) => item.value === locale));
  }, [locale]);

  useEffect(() => {
    if (typeof window === 'object') {
      window.addEventListener('click', handleListClick);
    }
    return () => window.removeEventListener('click', handleListClick);
  });

  const handleItemClick = (value) => {
    if (clicked === locale) {
      return;
    }
    setLocale(value);
    setIsShow(false);
    router.push(router.pathname, router.pathname, {
      locale: value,
      id: router.query.id,
    });
  };

  return (
    <div className={classNames('dropdown', className, { show: isShow })}>
      <DropdownButton defaultComponent={defaultComponent} />
      <div className="dropdown__list">
        {items.map(({ value, label }, index) => (
          <DropdownItem
            key={index}
            isActive={locale === value}
            onClick={() => handleItemClick(value)}
          >
            {label}
          </DropdownItem>
        ))}
      </div>
    </div>
  );
};

export default DropdownMain;

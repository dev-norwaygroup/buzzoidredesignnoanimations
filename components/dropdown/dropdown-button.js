/**
 * Internal Dependencies
 */
import { useDropdown } from './use-dropdown-context';
import IconChevronUpSmall from '@/svg/icon-chevron-up-small.svg';
import IconChevronDown from '@/svg/icon-chevron-down.svg';

const DropdownButton = ({ defaultComponent }) => {
  const { item, setIsShow, isShow } = useDropdown();

  return (
    <div
      className="dropdown__button"
      onClick={() => {
        setIsShow(!isShow);
      }}
    >
      <button className="dropdown__input">
        {item ? item.label : defaultComponent}
      </button>
      <div className="dropdown__icon-group">
        <IconChevronUpSmall />
        <IconChevronDown />
      </div>
    </div>
  );
};

export default DropdownButton;

/**
 * External Dependencies
 */
import classNames from 'classnames';
import { useEffect } from 'react';
import Link from 'next/link';
/**
 * Internal Dependencies
 */
import DropdownButton from './dropdown-button';
import DropdownItem from './dropdown-item';
import { useDropdown } from './use-dropdown-context';
import { Trans } from '@lingui/macro';

const dropdownItems = {
  0: {
    render: (
      <>
        <Link href="/blog/" passHref>
          <a>
            <Trans>Blog</Trans>
          </a>
        </Link>
      </>
    ),
    item: <Trans>Blog</Trans>,
  },
  4: {
    render: (
      <>
        <Link href="/blog/4" passHref>
          <a>
            <Trans>Instagram Followers</Trans>
          </a>
        </Link>
      </>
    ),
    item: <Trans>Instagram Followers</Trans>,
  },
  124: {
    render: (
      <>
        <Link href="/blog/124" passHref>
          <a>
            <Trans>Instagram Analytics</Trans>
          </a>
        </Link>
      </>
    ),
    item: <Trans>Instagram Analytics</Trans>,
  },
  161: {
    render: (
      <>
        <Link href="/blog/161" passHref>
          <a>
            <Trans>Instagram Marketing</Trans>
          </a>
        </Link>
      </>
    ),
    item: <Trans>Instagram Marketing</Trans>,
  },
};

const DropdownNavigation = ({ className, currentCategory = 0 }) => {
  const { setIsShow, isShow, setClicked } = useDropdown();
  const handleListClick = () => {
    if (isShow) {
      setIsShow(false);
    }
  };

  useEffect(() => {
    if (typeof window === 'object') {
      window.addEventListener('click', handleListClick);
    }
    return () => window.removeEventListener('click', handleListClick);
  });

  const handleItemClick = (index) => {
    setClicked(index);
    setIsShow(false);
  };

  const defaultComponent = dropdownItems[currentCategory].item;

  return (
    <div className={classNames('dropdown', className, { show: isShow })}>
      <DropdownButton defaultComponent={defaultComponent} />
      <div className="dropdown__list">
        {Object.entries(dropdownItems).map(([key, value]) => (
          <DropdownItem
            key={key}
            isActive={currentCategory == key}
            onClick={() => handleItemClick(key)}
          >
            {value.render}
          </DropdownItem>
        ))}
      </div>
    </div>
  );
};

export default DropdownNavigation;

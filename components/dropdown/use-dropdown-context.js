/**
 * External Dependencies
 */
 import { useContext, createContext, useState } from 'react';

 const DropdownContext = createContext();
 
 const DropDownProvider = (props) => {
   const [item, setItem] = useState(null);
   const [isShow, setIsShow] = useState(false);
   const [clicked, setClicked] = useState(0);

   const contextValue = {
     item,
     setItem,
     isShow,
     setIsShow,
     clicked,
     setClicked
   };
   
   return <DropdownContext.Provider value={contextValue} {...props} />;
 };

 const useDropdown = () => useContext(DropdownContext);

 export {
   DropDownProvider,
   useDropdown
 }
 
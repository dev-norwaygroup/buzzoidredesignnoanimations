/**
 * External Dependencies
 */
import classNames from 'classnames';

const DropdownItem = ({ children, isActive, onClick }) => (
  <li
    className={classNames('dropdown__item', { active: isActive })}
    onClick={onClick}
  >
    {children}
  </li>
);

export default DropdownItem;

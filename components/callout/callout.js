/**
 * External Dependencies
 */
import { Trans } from '@lingui/macro';
import classNames from 'classnames';

/**
 * Internal Dependencies
 */
import Button from "@/components/button/button";
import IconFigures from '@/svg/icon-figures.svg';
import IconFiguresGreen from '@/svg/icon-figures-green.svg';
import IconFiguresPurple from '@/svg/icon-figures-purple.svg';
import IconArrowLeft from '@/svg/icon-arrow-left.svg';
import IconArrowLeftSpace from '@/svg/icon-arrow-left-space.svg';

const Callout = ({
    color,
    fullText = false,
}) => {
    let IconFigure = <IconFigures />;
    let btn = <Button href="/automatic-instagram-likes" className="btn--white"><Trans>More</Trans></Button>;

    switch (color) {
        case 'green':
            IconFigure = <IconFiguresGreen />;
            break;
        case 'purple':
            IconFigure = <IconFiguresPurple />;
            break;
        case 'lightblue':
            IconFigure = <></>;
            btn = (
                <Button href="/automatic-instagram-likes" className="btn--clear btn--flex">
                    <Trans>Learn More </Trans>
                    <IconArrowLeftSpace />
                </Button>
            );
            break;
        case 'orange':
            IconFigure = <></>;
            btn = (
                <div className="callout__btn-group">
                    <Button href="/automatic-instagram-likes" className="btn--arrow btn--flex"><IconArrowLeft /></Button>
                    <Button href="/automatic-instagram-likes" className="btn--arrow btn--flex"><IconArrowLeft /></Button>
                    <Button href="/automatic-instagram-likes" className="btn--arrow btn--flex"><IconArrowLeft /></Button>
                </div>
            );
            break;
    };

    return (
        <div className={classNames('callout', color)}>
            <div className="shell">
                <div className="callout__inner">
                    <span>
                        {
                            fullText
                                ? <Trans>Try out our new Automatic Instagram Likes!</Trans>
                                : <Trans>New! Automatic Instagram Likes!</Trans>
                        }
                    </span>
                    {btn}
                </div>
            </div>
            <div className="callout__svg">
                {IconFigure}
            </div>
        </div>
    )
}

export default Callout;

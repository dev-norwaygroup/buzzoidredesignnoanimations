/**
 * External Dependencies
 */
import { Trans } from '@lingui/macro';

/**
 * Internal Dependencies
 */
import IconArrowRound from '@/svg/icon-arrow-round.svg';
import IconText from '@/components/icon-text/icon-text';
import { defaultIcons } from 'statics/icons';


const Offers = ({
    icons,
    children
}) => {
    let renderIcons = icons ? icons : defaultIcons;

    return (
        <div className='offers'>
            <div className="shell">
                <div className="offers__inner">
                    <div className="offers__heading">
                        <h2 className="h1-like">
                            <Trans>Take your Instagram to the next level</Trans>
                            <IconArrowRound />
                        </h2>
                    </div>
                    <div className="offers__entry">
                        <h2 className="h1-like">
                            <Trans>Take your Instagram to the next level</Trans>
                            <IconArrowRound />
                        </h2>
                        <div className="offers__icons">
                            {
                                renderIcons.map((x, index) => <IconText
                                    key={index}
                                    icon={x.icon}
                                    text={x.text}
                                />)
                            }
                        </div>
                    </div>
                    <div className="offers__content">{children}</div>
                </div>
            </div>
        </div>
    )
}

export default Offers;

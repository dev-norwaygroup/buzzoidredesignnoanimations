/**
 * External Dependencies
 */
import { Trans } from '@lingui/macro';

const servicesItems = [
  {
    content: <Trans>Buy Instagram Likes</Trans>,
    href: '/buy-instagram-likes',
  },
  {
    content: <Trans>Buy Instagram Followers</Trans>,
    href: '/buy-instagram-followers',
  },
  {
    content: <Trans>Buy Instagram Views</Trans>,
    href: '/buy-instagram-views',
  },
  {
    content: <Trans>Free Likes</Trans>,
    href: '/free-likes',
  },
];

const aboutUsItems = [
  {
    content: <Trans>FAQ</Trans>,
    href: '/faq',
  },
  {
    content: <Trans>Contact Us</Trans>,
    href: '/contact-us',
  },
  {
    content: <Trans>Blog</Trans>,
    href: '/blog',
  },
];

const buzzItems = [
  {
    content: <Trans>Log In</Trans>,
    href: '/login',
  },
  {
    content: <Trans>Sign Up</Trans>,
    href: '/sign-up',
  },
];

export { buzzItems, servicesItems, aboutUsItems };

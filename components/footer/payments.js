/**
 * Internal Dependencies
 */
import LogoPay from '@/svg/logo-pay.svg';
import LogoVisa from '@/svg/logo-visa.svg';
import LogoMastercard from '@/svg/logo-mastercard.svg';
import LogoAe from '@/svg/logo-ae.svg';
import LogoDc from '@/svg/logo-dc.svg';

const Payments = () => (
  <div className="payments">
    <div className="payments__inner">
      <LogoPay />
      <LogoVisa />
      <LogoMastercard />
      <LogoAe />
      <LogoDc />
    </div>
  </div>
);

export default Payments;

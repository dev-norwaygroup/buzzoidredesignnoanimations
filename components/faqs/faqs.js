/**
 * Internal Dependencies
 */
import Accordion from '@/components/accordion/accordion';
import { defaultItems } from '../../statics/faq';

const Faqs = ({ children, items }) => {
  return (
    <div className="faqs">
      {children && <div className="faqs__entry">{children}</div>}
      <Accordion items={items ? items : defaultItems} />
    </div>
  );
};

export default Faqs;

/**
 * External dependencies
 */
import { Trans } from '@lingui/macro';
import Image from 'next/image';

/**
 * Internal dependencies
 */
import Story1 from '@/svg/animate-story1.svg';
import Story2 from '@/svg/animate-story2.svg';
import Story1Image from '../../assets/images/story1-image.png';
import Story2Image from '../../assets/images/story2-image.png';
import Avatar1 from '../../assets/images/avatar1.jpg';
import Avatar2 from '../../assets/images/avatar2.jpg';
import AuthorBox from '@/components/author-box/author-box';
import Animate from '../animate/animate';
import Quote from '@/svg/quote.svg'
import Heading from '@/components/heading/heading';
import BoxRow from '@/components/box-row/box-row';

import AnimateStories from '@/svg/animate-stories.svg';

const CustomerStories = ({
    animateStories = true,
    title
}) => {
    return (
        <>
            <Animate>
                <Heading className="ta-c customer">
                    {animateStories && <AnimateStories />}
                    <h2>
                        {
                            title ? title : <Trans>Buzzoid Customer Stories</Trans>
                        }
                    </h2>
                </Heading>
            </Animate>
            <Animate>
                <BoxRow className="align-items-center mb box-row-story box-row-story--1">
                    <BoxRow.Col>
                        <div className="abs-image abs-image--story1">
                            <Story1 />
                            <div className="abs-image__item">
                                <Image src={Story1Image} width="247" height="237" alt="image" />
                            </div>
                        </div>
                    </BoxRow.Col>
                    <BoxRow.Col>
                        <Quote className="quote" />
                        <p className="large">
                            <Trans>
                                Simply love your services, helped me in growing my instagram and
                                every time I had some question everyone answered fast and any
                                issues was solved immediately! Keep up the good work.
                            </Trans>
                        </p>
                        <AuthorBox small avatar={Avatar2} name="@ariana_aria" verified>
                            <p className="is-orange">Verified Influencer</p>
                        </AuthorBox>
                    </BoxRow.Col>
                </BoxRow>
            </Animate>
            <Animate>
                <BoxRow className="align-items-center mb box-row-story box-row-story--2">
                    <BoxRow.Col>
                        <Quote className="quote" />
                        <p className="large">
                            <Trans>
                                Buzzoid has increased my visibility... I am now more marketable
                                in the social media realm...it works!
                            </Trans>
                        </p>
                        <AuthorBox small avatar={Avatar1} name="@mychalbella" verified>
                            <p className="is-orange">Verified Customer</p>
                        </AuthorBox>
                    </BoxRow.Col>
                    <BoxRow.Col>
                        <div className="abs-image abs-image--story2">
                            <Story2 />
                            <div className="abs-image__item">
                                <Image src={Story2Image} width="247" height="237" alt="image" />
                            </div>
                        </div>
                    </BoxRow.Col>
                </BoxRow>
            </Animate>
        </>
    )
}

export default CustomerStories;

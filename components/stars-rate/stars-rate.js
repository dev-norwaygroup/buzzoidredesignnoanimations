/**
 * External Dependencies
 */
import { useState, useMemo } from 'react';

/**
 * Internal Dependencies 
 */
import IconStarEmpty from '../../assets/images/svg/icon-star-empty.svg';
import IconStarFill from '../../assets/images/svg/icon-star-fill.svg';

const StarsRate = ({
    count,
    rating,
    onRating
}) => {
    const [hoverRating, setHoverRating] = useState(0);

    const isForFill = index => {
        if (rating >= index ) {
            return true;
        } else if (hoverRating < index) {
            return false;
        }  else {
            return true;
        }
    }

    const stars = useMemo(() => {
        return Array(count)
            .fill(0)
            .map((_, i) => i + 1)
            .map(index => (
                     <div
                        key={index}
                        onMouseEnter={() => setHoverRating(index)}
                        onMouseLeave={() => setHoverRating(0)}
                        onClick={() => onRating(index)}
                        >
                        { isForFill(index) ? <IconStarFill /> : <IconStarEmpty/> }
                    </div>
            ))
    }, [count, rating, hoverRating])
    return (
        <div className="stars-rate">
            <div className="stars-rate__stars">
                {stars}
            </div>
            <div className="stars-rate__text">{`${rating}/${count}`}</div>
        </div>
    )
}

export default StarsRate;

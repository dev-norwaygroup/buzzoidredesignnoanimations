import { useRouter } from 'next/router';

/**
 * Internal Dependencies
 */
import Header from '@/components/header/header';
import Footer from '@/components/footer/footer';
import Callout from '@/components/callout/callout';
import Head from 'next/head';

const Layout = ({ children }) => {
  const router = useRouter();
  const match = router.asPath.match(/\/([\w-]+)/);
  const uniqueClass = match ? match.pop() : 'home';
  return (
    <div className={uniqueClass}>
      <Head>
        <link rel="canonical" href="https://buzzoid.com/" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="Buzzoid" />
      </Head>
      <Callout />
      <Header />
      <div className="main-wrapper">{children}</div>
      <Footer />
    </div>
  );
};
export default Layout;
